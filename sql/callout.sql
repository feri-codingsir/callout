create database db_callout char set 'utf8mb4';
use db_callout;
DROP TABLE IF EXISTS t_user;
CREATE TABLE t_user(
                       id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                       phone VARCHAR(11)    COMMENT '手机号' ,
                       nickname VARCHAR(32)    COMMENT '昵称' ,
                       password VARCHAR(400)    COMMENT '密码' ,
                       invite_code VARCHAR(32)    COMMENT '邀请码' ,
                       flag INT   DEFAULT 1 COMMENT '标记位：1有效2无效' ,
                       imgurl VARCHAR(255)    COMMENT '头像地址' ,
                       utime DATETIME    COMMENT '更新时间' ,
                       ctime DATETIME    COMMENT '创建时间' ,
                       PRIMARY KEY (id)
)  COMMENT = '1.用户表';
DROP TABLE IF EXISTS t_user_log;
CREATE TABLE t_user_log(
                           id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                           uid INT    COMMENT '用户id' ,
                           type INT    COMMENT '类型 1注册2登录 3修改密码' ,
                           info VARCHAR(255)    COMMENT '操作信息' ,
                           ctime DATETIME    COMMENT '创建时间' ,
                           PRIMARY KEY (id)
)  COMMENT = '2.用户日志表';
DROP TABLE IF EXISTS t_sms_log;
CREATE TABLE t_sms_log(
                          id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                          phone VARCHAR(32)    COMMENT '手机号' ,
                          type INT    COMMENT '类型 1.登录 2.注册 3.密码找回' ,
                          info VARCHAR(255)    COMMENT '内容信息' ,
                          ctime DATETIME    COMMENT '创建时间' ,
                          PRIMARY KEY (id)
)  COMMENT = '3.短信发送记录';
-- 创建索引
create index i_userphone on t_user(phone,nickname,flag);
explain select * from t_user where phone ='sasa';
create index i_userloguid on t_user_log(uid);
explain select * from t_user_log where uid=1;
CREATE TABLE t_oss_log(
                          id INT NOT NULL AUTO_INCREMENT  COMMENT '序号' ,
                          bname VARCHAR(32)    COMMENT '存储空间名' ,
                          obj_name VARCHAR(255)    COMMENT '对象名' ,
                          pre_url VARCHAR(500)    COMMENT '预览地址' ,
                          ctime DATETIME    COMMENT '创建时间' ,
                          type INT    COMMENT '类型 1图片 2视频' ,
                          PRIMARY KEY (id)
)  COMMENT = '4.对象存储表';

DROP TABLE IF EXISTS t_user_inviter;
CREATE TABLE t_user_inviter(
                               id INT NOT NULL AUTO_INCREMENT  COMMENT '主键，序号' ,
                               uid INT    COMMENT '被邀请的人，用户id' ,
                               uiid INT    COMMENT '邀请人的用户id' ,
                               ctime DATETIME    COMMENT '创建时间' ,
                               PRIMARY KEY (id)
)  COMMENT = '5.用户邀请表';

CREATE TABLE t_user_wallet(
                              id INT NOT NULL AUTO_INCREMENT  COMMENT '租户号' ,
                              uid INT    COMMENT '乐观锁' ,
                              balance DECIMAL(24,2)    COMMENT '余额，单位元' ,
                              income DECIMAL(24,2)    COMMENT '收入，可提现，单位元' ,
                              total_balance DECIMAL(24,2)    COMMENT '总余额，单位元' ,
                              total_income DECIMAL(24,2)    COMMENT '总收入，单位元' ,
                              ctime DATETIME    COMMENT '创建时间' ,
                              utime DATETIME    COMMENT '更新时间' ,
                              PRIMARY KEY (id)
)  COMMENT = '6.用户钱包表';
DROP TABLE IF EXISTS t_user_vip;
CREATE TABLE t_user_vip(
                           id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                           uid INT    COMMENT '用户id' ,
                           start_date DATE    COMMENT '开始日期' ,
                           end_date DATE    COMMENT '结束日期' ,
                           days INT    COMMENT '总天数' ,
                           ctime DATETIME    COMMENT '创建时间' ,
                           utime DATETIME    COMMENT '更新时间' ,
                           PRIMARY KEY (id)
)  COMMENT = '7.用户vip表';

DROP TABLE IF EXISTS t_pay_log;
CREATE TABLE t_pay_log(
                          id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                          pay_type INT    COMMENT '支付类型 1支付宝 2微信 3银联' ,
                          no VARCHAR(32)    COMMENT '订单号' ,
                          pay_money DECIMAL(24,2)    COMMENT '支付金额，单位 元' ,
                          flag INT    COMMENT '标记位 1.未支付 2已支付 3退款' ,
                          ctime DATETIME    COMMENT '创建时间' ,
                          utime DATETIME    COMMENT '更新时间' ,
                          title VARCHAR(32)    COMMENT '支付标题' ,
                          uid INT    COMMENT '用户id' ,
                          type INT    COMMENT '操作类型：1.VIP充值 2.钱包充值' ,
                          PRIMARY KEY (id)
)  COMMENT = '8.支付记录表';

DROP TABLE IF EXISTS t_user_wallet;
CREATE TABLE t_user_wallet(
                              id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                              uid INT    COMMENT '用户id' ,
                              balance DECIMAL(24,2)    COMMENT '余额，单位元' ,
                              income DECIMAL(24,2)    COMMENT '收入，可提现，单位元' ,
                              total_balance DECIMAL(24,2)    COMMENT '总余额，单位元' ,
                              total_income DECIMAL(24,2)    COMMENT '总收入，单位元' ,
                              ctime DATETIME    COMMENT '创建时间' ,
                              utime DATETIME    COMMENT '更新时间' ,
                              PRIMARY KEY (id)
)  COMMENT = '9.用户钱包表';

drop TABLE t_coupon;
CREATE TABLE t_coupon(
                         id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                         title VARCHAR(32)    COMMENT '标题' ,
                         num INT    COMMENT '发行数量 如果为0 说明人人都有' ,
                         type INT    COMMENT '类型 1.无门槛劵 2.满减券 3.折扣券' ,
                         min_money INT    COMMENT '满减券 最低金额' ,
                         discount DECIMAL(24,6)    COMMENT '折扣' ,
                         end_date DATE    COMMENT '领取的结束日期' ,
                         flag INT    COMMENT '标记位 1 未审核 2审核通过 3审核拒绝' ,
                         form_date DATE    COMMENT '优惠券开始使用日期' ,
                         to_date DATE    COMMENT '优惠券开始结束日期' ,
                         ctype INT    COMMENT '优惠券类型1.VIP会员自动发放 2.VIP专属 3.所有用户' ,
                         send_type INT    COMMENT '发放：1.自动发放 2.主动领取' ,
                         info VARCHAR(255)    COMMENT '描述信息' ,
                         utime DATETIME    COMMENT '更新时间' ,
                         ctime DATETIME    COMMENT '创建时间' ,
                         img_url VARCHAR(255)    COMMENT '优惠券图片' ,
                         money DECIMAL(24,2)    COMMENT '抵扣的金额' ,
                         PRIMARY KEY (id)
)  COMMENT = '10.优惠券表';

DROP TABLE IF EXISTS t_user_coupon;

CREATE TABLE t_user_coupon(
                              id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                              uid INT    COMMENT '用户表id' ,
                              cid INT    COMMENT '优惠券id' ,
                              flag INT    COMMENT '标记位 1未使用 2已使用' ,
                              ctime DATETIME    COMMENT '更新时间' ,
                              utime DATETIME    COMMENT '创建时间' ,
                              PRIMARY KEY (id)
)  COMMENT = '11.用户优惠券表';

DROP TABLE IF EXISTS t_serve_type;
CREATE TABLE t_serve_type(
                             id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                             name VARCHAR(32)    COMMENT '类型名称' ,
                             info VARCHAR(255)    COMMENT '类型描述' ,
                             parent_id INT    COMMENT '上级id,如果为一级类型0，>0二级' ,
                             ctime DATETIME    COMMENT '创建时间' ,
                             level INT    COMMENT '级别 1、2、3' ,
                             PRIMARY KEY (id)
)  COMMENT = '12.服务类型表';

insert into t_serve_type
(name,info,parent_id,level,ctime) values
                                      ('家庭保洁','一站式卫生搞定',0,1,now()),
                                      ('上门维修','一站式维修',0,1,now()),
                                      ('上门按摩','轻松，舒服，爽',0,1,now()),
                                      ('家电清洗','省心',0,1,now()),
                                      ('搬家发货','一便宜实惠',0,1,now()),
                                      ('日常保洁','一站式卫生搞定',1,2,now()),
                                      ('深度保洁','一站式卫生搞定',1,2,now()),
                                      ('中式按摩','一站式卫生搞定',3,2,now()),
                                      ('油压SPA','一站式卫生搞定',3,2,now()),
                                      ('家电维修','一站式卫生搞定',2,2,now());


DROP TABLE IF EXISTS t_serve;
CREATE TABLE t_serve(
                        id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                        stid INT  COMMENT '类型id' ,
                        content text  COMMENT '服务内容' ,
                        imgurl VARCHAR(400)    COMMENT '图片' ,
                        detail VARCHAR(900)    COMMENT '服务详情' ,
                        ctime DATETIME    COMMENT '创建时间' ,
                        PRIMARY KEY (id)
)  COMMENT = '13.服务表';

insert into t_serve(stid,content,imgurl,detail,ctime) values
                                                          (6,'此服务携带基础清洁工具，不带吸尘器等特殊工具。
无额外收费，验收不满意免费返工，满意为止。
服务内容：
1. 参考时长：此项服务均为提前安排，不能确保能够现场加时，下单前请您根据房屋面积合理预估时长，100m²内建议订3小时，100-140m²建议订4小时，房屋较脏乱或需要洗碗请在上述基础上适当加时。
2. 服务区域：客厅/餐厅、卧室、厨房、卫生间、书房/储物间、阳台。
3. 服务项目：床面整理、台面物品整理、家具表面擦拭、室内门窗除尘(窗台及把手)、洗碗/厨具清洁、油烟机/灶台/厨电表面擦拭、五金件清洁、水池清洁、浴缸/淋浴房清洁、马桶清洁、镜子清洁、开关面板擦拭、阳台/楼梯扶手擦拭、全屋地面清洁、垃圾袋更换、垃圾打包带走。
服务须知：
1. 如有以下需求请在备注中说明：
· 需指定男/女保洁师；
· 需2名保洁师服务：请至少订购6小时服务（每人3小时，合计工作时长6小时）；
2. 加时&退款说明：
· 现场加时按45元/小时在线补差价，切勿线下付款；
· 如单次服务剩余时长超半小时，可按比例申请退款；
3. 此服务不包含：
家电/家具/橱柜内部、室外玻璃/纱窗/百叶窗/防护窗、窗帘、古董字画、宗教陈设、花草绿植、天花板、灯具、装修后未入住新房、重油污房、超1个月未居住房、自建房、群租房清洁、室外/高空作业。',
                                                           'https://img1.daoway.cn/img/price_head/2023/07/17/2581706.jpg','https://img1.daoway.cn/img/price_detail/2022/01/07/18eb5db2-2623-477a-8aa0-cc521ed95112.jpg',
                                                           now()),
                                                          (8,'服务时长：70分钟

理疗流程：
仰/俯卧位(头部、肩颈、腹部、腰背、四肢按摩)

功效及适宜人群：
1.缓解肌肉劳损、疏通经络、改善睡眠、提高免疫力；
2.适用于因工作压力大、缺乏锻炼、经常熬夜导致身体疲乏、腰酸背痛的人群。

服务禁忌：
1.血液病患者、急性软组织损伤者、严重心/肺/肝功能衰竭者、恶性贫血者、重度高血压者、传染病患者、胃肠穿孔者、皮肤病变、皮炎、急性炎症高热不退者禁做；
2.诊断不明急性脊柱损伤或伴有脊髓症状患者（如骨折、骨裂和颈椎脱位等），骨髓炎、骨关节结核、严重的骨质疏松症者禁做；
3.经期、孕期女性禁做，70岁以上老人禁做；
4.到位平台只提供专业绿色正规服务，对不正当行为和要求，服务人员有权拒绝服务并保留诉诸法律的权利；

温馨提示：
1. 推拿按摩后出现局部疼痛属于正常情况，一般1-3天疼痛症状会慢慢消失；
2. 推拿按摩会加快身体新陈代谢，建议在服务结束后喝杯温开水；
3. 推拿按摩结束后不宜立即沐浴，休息1个小时后再沐浴，以免对扩张的毛孔造成刺激，引起气血流通不畅；
4. 推拿按摩前后各1小时不宜进食，以免引起身体不适；

用品提供：
口罩、鞋套、一次性床单、一次性按摩巾',
                                                           'https://img1.daoway.cn/img/price_head/2023/07/14/2574959.jpg','https://img1.daoway.cn/img/price_detail/2022/09/14/7605ac4b-1a9c-4e55-9743-decce2f6540e.jpg',
                                                           now())
;
update t_serve set stid=8 where id=2;

DROP TABLE IF EXISTS t_serve_detail;
CREATE TABLE t_serve_detail(
                               id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                               sid INT    COMMENT '服务id' ,
                               price DECIMAL(24,2)    COMMENT '原价格' ,
                               curr_price DECIMAL(24,2)    COMMENT '平台价格' ,
                               name VARCHAR(32)    COMMENT '服务名称' ,
                               specs VARCHAR(255)    COMMENT '规格，价格，时长等' ,
                               ctime DATETIME    COMMENT '创建时间' ,
                               flag INT    COMMENT '标记位 1未上架 2上架' ,
                               PRIMARY KEY (id)
)  COMMENT = '14.服务详情表';

DROP TABLE IF EXISTS t_cart;
CREATE TABLE t_cart(
                       id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                       uid INT    COMMENT '用户id' ,
                       sdid INT    COMMENT '服务id' ,
                       num INT    COMMENT '数量' ,
                       join_price DECIMAL(24,2)    COMMENT '加入时价格' ,
                       shop VARCHAR(32)    COMMENT '名称' ,
                       ctime DATETIME    COMMENT '创建时间' ,
                       utime DATETIME    COMMENT '更新时间' ,
                       PRIMARY KEY (id)
)  COMMENT = '15.购物车表';

DROP TABLE IF EXISTS t_user_address;
CREATE TABLE t_user_address(
                               id VARCHAR(32)    COMMENT '序号，主键' ,
                               uid VARCHAR(32)    COMMENT '用户id' ,
                               address VARCHAR(32)    COMMENT '服务地址' ,
                               house DATETIME    COMMENT '门牌号' ,
                               flag VARCHAR(32)    COMMENT '排序' ,
                               ctime DATETIME    COMMENT '创建时间' ,
                               name VARCHAR(255)    COMMENT '联系人' ,
                               phone VARCHAR(255)    COMMENT '手机号' ,
                               sex VARCHAR(255)    COMMENT '性别：先生 女士'
)  COMMENT = '16.用户服务地址';

DROP TABLE IF EXISTS t_order;
CREATE TABLE t_order(
                        id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                        uid INT    COMMENT '用户id' ,
                        title VARCHAR(32)    COMMENT '订单标题' ,
                        money DECIMAL(24,2)    COMMENT '订单金额' ,
                        pay_money DECIMAL(24,2)    COMMENT '支付金额' ,
                        info VARCHAR(255)    COMMENT '订单备注' ,
                        uaid INT    COMMENT '用户服务地址' ,
                        sidd INT    COMMENT '服务产品id' ,
                        s_time DATETIME    COMMENT '服务时间' ,
                        ucid INT    COMMENT '用户优惠券' ,
                        pay_type INT    COMMENT '字典表-支付方式支付宝 、微信 、银联' ,
                        no VARCHAR(32)    COMMENT '订单号' ,
                        flag INT    COMMENT '字典表-订单状态：未支付 、已支付，未服务、已服务，未评价 、已评价 、取消订单、支付超时 、退款' ,
                        ctime DATETIME    COMMENT '创建时间' ,
                        utime DATETIME    COMMENT '更新时间' ,
                        num INT    COMMENT '服务数量' ,
                        simg VARCHAR(500)    COMMENT '服务产品的图片' ,
                        sprice DECIMAL(24,2)    COMMENT '服务产品价格' ,
                        specs VARCHAR(32)    COMMENT '服务产品规格' ,
                        PRIMARY KEY (id)
)  COMMENT = '17.订单表';

DROP TABLE IF EXISTS t_order_log;
CREATE TABLE t_order_log(
                            id INT NOT NULL AUTO_INCREMENT  COMMENT '序号，主键' ,
                            oid INT    COMMENT '订单id' ,
                            type INT    COMMENT '字典表-订单状态' ,
                            info VARCHAR(255)    COMMENT '备注信息' ,
                            ctime DATETIME    COMMENT '创建时间' ,
                            PRIMARY KEY (id)
)  COMMENT = '18.订单流水表';


insert into t_serve_detail(sid,price,curr_price,name,specs,ctime,flag) values
                                                                           (1,235,135,'3小时保洁(100㎡内)','元/3小时',now(),2),
                                                                           (1,199,99,'2小时保洁(60㎡内)','元/2小时',now(),2),
                                                                           (1,275,175,'4小时保洁(140㎡内)','元/4小时',now(),2),
                                                                           (1,415,215,'5小时保洁(180㎡内)','元/5小时',now(),2),
                                                                           (2,338,218,'全身推拿','70分钟',now(),2);

