package com.feri.co.common.dto;

import lombok.Data;

import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/12 11:40
 */
@Data
public class CartDetailDto {
    private Integer cid;
    //用户id
    private Integer cuid;
    //服务id
    private Integer sdid;
    //数量
    private Integer cnum;
    //加入时价格
    private Double cjprice;
    //名称
    private String shop;
    //更新时间
    private Date utime;
    private Integer id;
    //类型id
    private Integer stid;
    //服务内容
    private String content;
    //图片
    private String imgurl;
    //服务详情
    private String detail;
    private Integer sid;
    //原价格
    private Double price;
    //平台价格
    private Double currPrice;
    //服务名称
    private String name;
    //规格，价格，时长等
    private String specs;
    //标记位 1未上架 2上架
    private Integer flag;
}
