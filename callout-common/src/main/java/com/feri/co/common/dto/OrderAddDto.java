package com.feri.co.common.dto;

import lombok.Data;

import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/12 10:59
 */
@Data
public class OrderAddDto {
    //订单标题
    private String title;
    //购物车id
    private int cid;
    //订单金额
    private Double money;
    //支付金额
    private Double payMoney;
    //订单备注
    private String info;
    //用户服务地址
    private Integer uaid;
    //服务产品id
    private Integer sdid;
    //服务时间
    private Date sTime;
    //用户优惠券
    private Integer ucid;
    //字典表-支付方式 支付宝 、微信 、银联、钱包
    private Integer payType;
    //服务数量
    private Integer num;
    //技术id
    private Integer tid;
    //服务产品的图片
    private String simg;
    //服务产品价格
    private Double sprice;
    //服务产品规格
    private String specs;
    //是否为vip
    private boolean vip;
}
