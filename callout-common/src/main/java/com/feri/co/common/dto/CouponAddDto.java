package com.feri.co.common.dto;

import lombok.Data;

import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/10 15:11
 */
@Data
public class CouponAddDto {
    //发放：1.自动发放 2.主动领取
    private Integer sendType;
    //折扣
    private Double discount;
    //标题
    private String title;
    //发行数量 如果为0 说明人人都有
    private Integer num;
    //类型 1.无门槛劵 2.满减券 3.折扣券
    private Integer type;
    //满减券 最低金额
    private Integer minMoney;
    //抵扣的金额
    private Integer money;
    //领取的结束日期
    private Date endDate;
    //优惠券开始使用日期
    private Date formDate;
    //优惠券开始结束日期
    private Date toDate;
    //优惠券类型1.VIP会员自动发放 2.VIP专属 3.所有用户
    private Integer ctype;
    //描述信息
    private String info;
    private String imgUrl;
}
