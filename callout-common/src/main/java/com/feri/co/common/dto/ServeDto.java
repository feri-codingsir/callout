package com.feri.co.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/11 16:14
 */
@Data
public class ServeDto implements Serializable {
    private Integer id;
    //类型id
    private Integer stid;
    //类型名称
    private String stname;
    //服务内容
    private String content;
    //图片
    private String imgurl;
    //服务详情
    private String detail;
    //创建时间
    private Date ctime;
    private Integer sdid;
    //原价格
    private Double price;
    //平台价格
    private Double currPrice;
    //服务名称
    private String name;

    //规格，价格，时长等
    private String specs;
}
