package com.feri.co.common.config;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/8 14:17
 */
public interface RedisKeyConfig {
    /**
     * 记录注册验证码
     * String类型
     * key追加手机号 value 验证码
     * 有效期 10分钟*/
    String SMS_RCODE="co:sms:rcode:";
    /**
     * 记录登录验证码
     * String类型
     * key追加手机号 value 验证码
     * 有效期 10分钟*/
    String SMS_LCODE="co:sms:lcode:";
    /**
     * 记录密码验证码
     * String类型
     * key追加手机号 value 验证码
     * 有效期 10分钟*/
    String SMS_FCODE="co:sms:fcode:";
    /**
     * 记录校验通过的注册的手机号
     * Set类型
     * 没有有效期*/
    String CHECKED_PHONES="co:sms:checked";

    /**
     * 记录令牌
     * String类型
     * 12小时*/
    String AUTH_TOKEN="co:auth:token:";
    /**
     * 记录在线的账号
     * String类型
     * 12小时
     * */
    String AUTH_LOGIN="co:auth:uid:";

    /**
     * 记录可以领取的优惠券
     * Hash类型
     * */
    String COUPONS="co:coupons";

    /**
     * 记录服务的类型
     * List类型
     * */
    String SERVE_TYPES="co:servetypes";

    /**
     * 记录服务的绑定
     * 根据类型动态拼接*
     * List类型
     * 拼接：一级类型的id
     * value 存储对应榜单内容 排好
     * 日榜单 有效期 今日剩余时间 就可以不用写任务调度框架
     * */
    String SERVE_TOP="co:servetop:";

}
