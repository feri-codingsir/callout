package com.feri.co.common.config;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/8 14:17
 */
public interface SystemConfig {

    /**
     * 用户日志操作类型 1注册*/
    Integer UL_ADD=1;
    /**
     * 用户日志操作类型 2登录*/
    Integer UL_LOGIN=2;
    /**自定义请求消息头
     * 记录令牌*/
    String HEADER_TOKEN="co-token";

    /**
     * VIP充值的天数*/
    int VIP_DAYS=365;
    /**
     * 自动发送优惠券 ，单个任务的最大数量*/
    int COUPONS_ADDS=100000;

    /**
     * VIP 折扣*/
    double VIP_FREE=0.95;

}
