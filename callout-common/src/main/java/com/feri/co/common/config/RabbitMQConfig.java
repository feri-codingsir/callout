package com.feri.co.common.config;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/8 14:17
 */
public interface RabbitMQConfig {
    /**
     * 队列
     * 记录需要发送的短信验证码内容
     * */
    String SMS_QUEUE="co-sms-code";
    /**
     * 队列
     * 记录需要发放优惠券*/
    String COUPONS_SEND="co-coupons-send";
    /**
     * 队列
     * 记录需要同步优惠券*/
    String COUPONS_SYNC="co-coupons-sync";

    /**
     * 队列
     * 记录订单信息，设置有效期
     * 目的实现订单超时未支付，自动取消*/
    String TTL_QUEUE="co-queue-orderttl";
    /**
     * 队列
     * 获取死信交换器转发的死信消息*/
    String DEAD_QUEUE="co-queue-orderdead";

    /**
     * 交换器
     * 死信交换器，专门用来转发死信消息*/
    String DEAD_EXCHANGE="co-order-deadex";

    /**
     * 队列
     * 记录要退款的订单信息
     * */
    String ORDER_R_QUEUE="co-order-refound";
}
