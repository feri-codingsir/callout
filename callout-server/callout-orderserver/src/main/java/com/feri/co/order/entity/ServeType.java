package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 12.服务类型表(ServeType)表实体类
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@Data
@TableName("t_serve_type")
public class ServeType {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //类型名称
    private String name;
    //类型描述
    private String info;
    //上级id,如果为一级类型0，>0二级
    private Integer parentId;
    //创建时间
    private Date ctime;
    //级别 1、2、3
    private Integer level;


}
