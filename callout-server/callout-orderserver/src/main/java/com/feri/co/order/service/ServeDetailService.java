package com.feri.co.order.service;

import com.feri.co.common.dto.ServeDetailAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.ServeDetail;
/**
 * 14.服务详情表(ServeDetail)表服务接口
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
public interface ServeDetailService{
    /**
     * 新增*/
    R save(ServeDetailAddDto dto);
    /**
     * 修改*/
    R update(ServeDetail detail);
    /**
     * 上下架*/
    R updateFlag(int id,int flag);
    /**
     * 删除 假删除 逻辑删除 修改标记位*/
    R del(int id);
}

