package com.feri.co.order.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.*;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.*;
import com.feri.co.order.entity.Order;
import com.feri.co.order.entity.OrderLog;
import com.feri.co.order.entity.UserAddress;
import com.feri.co.order.feign.PayLogService;
import com.feri.co.order.feign.UserCouponService;
import com.feri.co.order.feign.UserVipService;
import com.feri.co.order.feign.UserWalletService;
import com.feri.co.order.service.OrderService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 17.订单表(Order)表服务实现类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao dao;
    @Resource
    private OrderLogDao logDao;

    @Resource
    private CartDao cartDao;
    @Resource
    private ServeDao serveDao;
    @Resource
    private UserAddressDao addressDao;
    @Resource
    private UserCouponService couponService;
    @Resource
    private UserWalletService walletService;
    @Resource
    private UserVipService vipService;
    @Resource
    private PayLogService payLogService;
    @Resource
    private StringRedisTemplate template;
    @Resource
    private RabbitTemplate rabbitTemplate;
    /**
     * 分布式事务：Seata XA模式  微服务 服务嵌套调用 */
    @Override
    public R buy(OrderAddDto dto, String token) {
        //1.校验
        if(dto!=null){
            String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
            //2.计算订单金额
            ServeDto serve=serveDao.selectBySdId(dto.getSdid());
            if(serve!=null){
                //服务存在，才可以下单
                Order order=new Order();
                BeanUtils.copyProperties(dto,order);
                order.setMoney(serve.getCurrPrice()*dto.getNum());
                double freeMoney=0;
                //3.计算优惠金额
                if(dto.isVip()){
                    //是vip 享受vip专属价 95折
                    freeMoney+=order.getMoney()* SystemConfig.VIP_FREE;
                }
                boolean cou=false;
                if(dto.getUcid()>0){
                    //验证本订单是否使用优惠券
                    UserCouponDto coupon=couponService.coupon(dto.getUcid());
                    if(coupon!=null){
                        if(diffDay(coupon.getEndDate())>=0){
                            cou=true;
                            //有效 计算金额
                            if(coupon.getType()==1){
                                //无门槛
                                freeMoney+=coupon.getMoney();
                            }else if(coupon.getType()==2){
                                //满减券
                                if(order.getMoney()>=coupon.getMinMoney()){
                                    freeMoney+=coupon.getMoney();
                                }
                            }else {
                                freeMoney+=order.getMoney()*coupon.getDiscount();
                            }
                        }
                    }
                }
                order.setPayMoney(order.getMoney()-freeMoney);
                //如果钱包支付，需要校验 钱包余额
                if(dto.getPayType()==4){
                    //钱包支付
                    if(walletService.money(uid)<order.getPayMoney()){
                        //余额 不够
                        return R.fail("亲，余额不足，快来充值！");
                    }
                }
                order.setNo(System.currentTimeMillis()+"-"+uid+dto.getSdid());
                order.setFlag(5);
                order.setUid(Integer.parseInt(uid));
                order.setCtime(new Date());
                order.setUtime(new Date());
                //新增订单
                if(dao.insert(order)>0){
                    //如果存在优惠券 更改优惠券状态
                    if(cou){
                        //更改优惠券状态
                        couponService.updateFlag(dto.getUcid(),2);
                    }
                    //如果是钱包扣款 更改钱包余额
                    if(dto.getPayType()==4){
                        walletService.updateMoney(order.getPayMoney(),order.getUid());
                    }else {
                        payLogService.add(new PayAddDto(dto.getPayType(),order.getPayMoney(),dto.getTitle(),order.getNo(),uid));
                        //支付的订单 10分钟必须付款，如果未付款 自动超时 rabbitMQ 的 死信
                        rabbitTemplate.convertAndSend("", RabbitMQConfig.TTL_QUEUE,order.getId());
                    }
                    //如果是购物车下单，需要回删购物车数据
                    if(dto.getCid()>0){
                        cartDao.deleteById(dto.getCid());
                    }
                    logDao.insert(new OrderLog(order.getId(),order.getFlag(),"新增订单服务"));
                    return R.ok();
                }
            }
        }
        return R.fail();
    }

    @Override
    public R update(String no, int flag) {
        //更改订单状态 1.支付成功 2.开始服务 3服务结束，未评价 4.评价完成
        if(dao.updateFlag(no,flag)>0){
            Order order=dao.selectOne(new LambdaQueryWrapper<Order>().eq(Order::getNo,no));
            switch (flag){
                case 6://支付成功
                    //发起MQ 发送服务消息  上单App（服务人员）中 接收到消息，生成任务，供服务人员抢单
                    break;
                case 7://服务进行中
                    //开启监听，监听意外情况
                    break;
                case 8://服务完成，未评价
                    //实现服务人员佣金分成
                    break;
                case 9://已评价
                    //给予奖励

                    break;
            }
            logDao.insert(new OrderLog(order.getId(),order.getFlag(),"状态发送改变！"));
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R queryByUid(String token) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        LambdaQueryWrapper<Order> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Order::getUid,uid);
        wrapper.orderByDesc(Order::getCtime);
        return R.ok(dao.selectList(wrapper));
    }

    @Override
    public R queryPre(int type, int id,String token) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        Map<String,Object> map=new HashMap<>();
        //1.查询对应的服务详情
        ServeDto serve=null;
        //区分 是立即下单还是购物车下单
        if(type==1){
            //详情 立即预约
            serve=serveDao.selectBySdId(id);
        }else {
            //购物车
            serve=serveDao.selectByCid(id);
        }
        map.put("serve",serve);
        //2.查询服务地址 默认
        LambdaQueryWrapper<UserAddress> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserAddress::getUid,uid).eq(UserAddress::getFlag,1);
        UserAddress address=addressDao.selectOne(wrapper);
        map.put("address",address);
        //3.优惠券
        map.put("coupons",couponService.access(uid));
        //4.vip
        map.put("vip",vipService.checkVip(uid));

        return R.ok(map);
    }

    @Override
    public R cancelOrder(String no) {
        LambdaQueryWrapper<Order> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Order::getNo,no);
        Order order=dao.selectOne(wrapper);
        if(order!=null){
            if(order.getFlag()==5){
                //未支付的才可以取消
                if(dao.updateFlag(no,9)>0){
                    //订单取消，需要释放订单中的优惠券、余额
                    //钱包
                    if(order.getPayType()==4){
                        //钱包余额支付 退款
                        walletService.updateMoney(order.getPayMoney(),order.getUid());
                    }
                    //优惠券
                    if(order.getUcid()>0){
                        //优惠券需要退回，前提是优惠券未失效
                        UserCouponDto couponDto=couponService.coupon(order.getUcid());
                        if(diffDay(couponDto.getEndDate())>=0){
                            //有效 则退回
                            couponService.updateFlag(couponDto.getUcid(),1);
                        }
                    }
                }
            }
        }
        return R.fail();
    }

    @Override
    public R refoundOrder(String no, String reason) {
        LambdaQueryWrapper<Order> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Order::getNo,no);
        Order order=dao.selectOne(wrapper);
        if(order!=null){
            if(order.getFlag()==6){
                //已支付，才可以退款，但是已服务就不行了
                //服务前2小时不扣手续费，小于2小时但是未开始，扣30%作为损失费用
                long hour=diffHour(order.getSTime());
                double money=0;
                if(hour>2){
                    //不扣手续费
                    money=order.getPayMoney();
                }else if(hour>0){
                    //扣手续费 30%
                    money=order.getPayMoney()*0.7;
                }else {
                    return R.fail("亲，已经开始的服务，是不能退款，有任何问题，可以联系我们的客户！");
                }
                if(money>0){
                    if(dao.updateFlag(order.getNo(),11)>0){
                        //发送 MQ  异步处理订单的退款
                        rabbitTemplate.convertAndSend("",RabbitMQConfig.ORDER_R_QUEUE,
                                new OrderRefoundDto(order.getPayType(),order.getNo(),money,order.getTitle()+"-退款",order.getUid()));
                        logDao.insert(new OrderLog(order.getId(),11,"发起了退款"));
                        return R.ok();
                    }
                }
            }
        }
        return R.fail();
    }
    private long diffDay(Date d1){
        LocalDateTime start = LocalDateTimeUtil.now();
        LocalDateTime end = LocalDateTimeUtil.of(d1);
        return LocalDateTimeUtil.between(start, end).toDays();
    }
    private long diffHour(Date d1){
        LocalDateTime start = LocalDateTimeUtil.now();
        LocalDateTime end = LocalDateTimeUtil.of(d1);
        return LocalDateTimeUtil.between(start, end).toHours();
    }
}

