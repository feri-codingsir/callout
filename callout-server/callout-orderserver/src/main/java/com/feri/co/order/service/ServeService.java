package com.feri.co.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.common.dto.ServeAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.Serve;

/**
 * 13.服务表(Serve)表服务接口
 *
 * @author Feri
 * @since 2023-08-11 11:46:15
 */
public interface ServeService{

    /**
     * 新增
     **/
    R save(ServeAddDto dto);
    /**
     * 修改
     **/
    R update(Serve serve);
    /**
     * 删除
     **/
    R del(int id);
    /**
     * 查询 首页榜单*/
    R queryTop(int stid);
    /**
     * 查询 根据类型 要是动态排序 分页*/
    R queryList(int level,int type,String order);
    /**
     * 查询 详情 热门服务 缓存*/
    R queryDetail(int id);
}

