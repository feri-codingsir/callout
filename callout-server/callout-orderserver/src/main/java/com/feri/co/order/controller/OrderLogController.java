package com.feri.co.order.controller;

import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import com.feri.co.order.service.OrderLogService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * 18.订单流水表(OrderLog)表控制层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@RestController
@RequestMapping("/server/orderlog/")
public class OrderLogController{
    /**
     * 服务对象
     */
    @Resource
    private OrderLogService service;
    
    @GetMapping("logs")
    public R logs(@RequestBody PageBo bo){
        return service.page(bo);
    }
}

