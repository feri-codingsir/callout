package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 13.服务表(Serve)表实体类
 *
 * @author Feri
 * @since 2023-08-11 11:46:15
 */
@Data
@TableName("t_serve")
public class Serve {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //类型id
    private Integer stid;
    //服务内容
    private String content;
    //图片
    private String imgurl;
    //服务详情
    private String detail;
    //创建时间
    private Date ctime;
}
