package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 14.服务详情表(ServeDetail)表实体类
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@Data
@TableName("t_serve_detail")
public class ServeDetail {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //服务id
    private Integer sid;
    //原价格
    private Double price;
    //平台价格
    private Double currPrice;
    //服务名称
    private String name;
    //规格，价格，时长等
    private String specs;
    //创建时间
    private Date ctime;
    //标记位 1未上架 2上架
    private Integer flag;
}
