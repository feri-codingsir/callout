package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.order.entity.ServeDetail;

/**
 * 14.服务详情表(ServeDetail)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
public interface ServeDetailDao extends BaseMapper<ServeDetail> {

}

