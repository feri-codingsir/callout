package com.feri.co.order.service.impl;

import com.feri.co.common.dto.ServeDetailAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.ServeDetailDao;
import com.feri.co.order.entity.ServeDetail;
import com.feri.co.order.service.ServeDetailService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;

/**
 * 14.服务详情表(ServeDetail)表服务实现类
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@Service
public class ServeDetailServiceImpl implements ServeDetailService {
    @Resource
    private ServeDetailDao dao;

    @Override
    public R save(ServeDetailAddDto dto) {
        ServeDetail detail=new ServeDetail();
        BeanUtils.copyProperties(detail,detail);
        detail.setCtime(new Date());
        detail.setFlag(1);
        if(dao.insert(detail)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R update(ServeDetail detail) {
        if(dao.updateById(detail)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R updateFlag(int id, int flag) {
        ServeDetail detail=new ServeDetail();
        detail.setFlag(flag);
        detail.setId(id);
        if(dao.updateById(detail)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R del(int id) {
        if(dao.deleteById(id)>0){
            return R.ok();
        }
        return R.fail();
    }
}

