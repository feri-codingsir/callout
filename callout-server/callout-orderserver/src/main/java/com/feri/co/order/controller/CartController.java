package com.feri.co.order.controller;


import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.CartAddDto;
import com.feri.co.common.dto.CartUpdateNumDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.service.CartService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 15.购物车表(Cart)表控制层
 *
 * @author Feri
 * @since 2023-08-12 10:19:48
 */
@RestController
@RequestMapping("/server/cart/")
public class CartController {
    /**
     * 服务对象
     */
    @Resource
    private CartService service;


    @PostMapping("add")
    R save(@RequestBody CartAddDto dto, HttpServletRequest request){
        return service.save(dto,request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    /**
     * 修改数量*/
    @PostMapping("change")
    R updateNum(@RequestBody CartUpdateNumDto dto){
        return service.updateNum(dto);
    }
    /**
     * 查询某个用户购物车*/
    @PostMapping("my")
    R queryByUid(HttpServletRequest request){
        return service.queryByUid(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    /**
     * 删除 购物车内容*/
    @GetMapping("del")
    R del(int id){
        return service.del(id);
    }
}
