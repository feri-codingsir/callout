package com.feri.co.order.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.ServeAddDto;
import com.feri.co.common.dto.ServeDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.ServeDao;
import com.feri.co.order.entity.Serve;
import com.feri.co.order.service.ServeService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 13.服务表(Serve)表服务实现类
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@Service
public class ServeServiceImpl implements ServeService {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private ServeDao dao;

    @Override
    public R save(ServeAddDto dto) {
        Serve serve=new Serve();
        BeanUtils.copyProperties(dto,serve);
        serve.setCtime(new Date());
        if(dao.insert(serve)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R update(Serve serve) {
        if(dao.updateById(serve)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R del(int id) {
        if(dao.deleteById(id)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R queryTop(int stid) {
        //榜单 Redis(List Zset Hash) key不设置有效期
        if(Boolean.TRUE.equals(redisTemplate.hasKey(RedisKeyConfig.SERVE_TOP + stid))){
            return R.ok(redisTemplate.opsForList().range(RedisKeyConfig.SERVE_TOP+stid,0,redisTemplate.opsForList().size(RedisKeyConfig.SERVE_TOP+stid)));
        }else {
            List<ServeDto> list=dao.selectTop(stid);
            redisTemplate.opsForList().leftPushAll(RedisKeyConfig.SERVE_TOP+stid,list);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            LocalDateTime localDateTime = LocalDateTimeUtil.parse(sdf.format(new Date())+"T23:59:59");
            LocalDateTime endOfDay = LocalDateTimeUtil.endOfDay(localDateTime);
            //设置有效期 今日剩余的秒数
            redisTemplate.expire(RedisKeyConfig.SERVE_TOP+stid,endOfDay.getSecond()-System.currentTimeMillis()/1000, TimeUnit.SECONDS);
            return R.ok(list);
        }
    }

    @Override
    public R queryList(int level,int type,String order) {
        String t,o;
        if(level==1){
            //一级
            t="st.parent_id="+type;
        }else {
            //二级
            t="st.id="+type;
        }
        o="sd."+order;

        return R.ok(dao.selectList(t,o));
    }

    @Override
    public R queryDetail(int id) {
        return R.ok(dao.selectById(id));
    }
}

