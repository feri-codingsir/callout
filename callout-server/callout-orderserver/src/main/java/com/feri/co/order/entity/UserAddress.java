package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 16.用户服务地址(UserAddress)表实体类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Data
@TableName("t_user_address")
public class UserAddress{
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //服务地址
    private String address;
    //门牌号
    private String house;
    //排序 1 默认地址 其余 不是默认
    private Integer flag;
    //创建时间
    private Date ctime;
    private Date utime;
    //联系人
    private String name;
    //手机号
    private String phone;
    //性别：先生 女士
    private String sex;
}
