package com.feri.co.order.controller;

import com.feri.co.common.bo.PageBo;
import com.feri.co.common.dto.ServeTypeAddDto;
import com.feri.co.common.dto.ServeTypeDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.ServeType;
import com.feri.co.order.service.ServeTypeService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Map;

/**
 * 12.服务类型表(ServeType)表控制层
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@RestController
@RequestMapping("/server/servetype/")
public class ServeTypeController {
    /**
     * 服务对象
     */
    @Resource
    private ServeTypeService service;
    @PostMapping("save")
    public R save(@RequestBody ServeTypeAddDto dto){
        return service.save(dto);
    }
    @PostMapping("update")
    public R update(@RequestBody ServeType type){
        return service.update(type);
    }
    @PostMapping("del")
    public R del(@RequestBody Map<String,Integer> map){
        return service.del(map.get("id"));
    }
    @GetMapping("home")
    public R home(){
        return service.queryHome();
    }
    @PostMapping("page")
    public R page(@RequestBody PageBo bo){
        return service.queryPage(bo);
    }
}
