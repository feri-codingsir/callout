package com.feri.co.order.service;

import com.feri.co.common.dto.UserAddressAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.UserAddress;

/**
 * 16.用户服务地址(UserAddress)表服务接口
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface UserAddressService {
    /**
     * 新增*/
    R save(UserAddressAddDto dto,String token);
    /**
     * 修改*/
    R update(UserAddress address);
    /**
     * 查询全部*/
    R all();
    /**
     * 查询默认*/
    R defaultOne(String token);
}
