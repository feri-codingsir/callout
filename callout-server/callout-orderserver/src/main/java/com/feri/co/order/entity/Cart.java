package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 15.购物车表(Cart)表实体类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Data
@TableName("t_cart")
public class Cart{
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //服务id
    private Integer sdid;
    //数量
    private Integer num;
    //加入时价格
    private Double joinPrice;
    //名称
    private String shop;
    //创建时间
    private Date ctime;
    //更新时间
    private Date utime;
}
