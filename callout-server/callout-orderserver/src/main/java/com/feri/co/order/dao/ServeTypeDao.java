package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.common.dto.ServeTypeDto;
import com.feri.co.order.entity.ServeType;

import java.util.List;

/**
 * 12.服务类型表(ServeType)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
public interface ServeTypeDao extends BaseMapper<ServeType> {

    List<ServeTypeDto> selectHome();
}


