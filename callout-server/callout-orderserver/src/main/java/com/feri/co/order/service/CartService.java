package com.feri.co.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.common.dto.CartAddDto;
import com.feri.co.common.dto.CartUpdateNumDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.Cart;

import javax.annotation.Resource;

/**
 * 15.购物车表(Cart)表服务接口
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface CartService{
    /**
     * 添加购物车 新增或修改*/
    R save(CartAddDto dto,String token);
    /**
     * 修改数量*/
    R updateNum(CartUpdateNumDto dto);
    /**
     * 查询某个用户购物车*/
    R queryByUid(String token);
    /**
     * 删除 购物车内容*/
    R del(int id);

}

