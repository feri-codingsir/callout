package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.order.entity.OrderLog;

/**
 * 18.订单流水表(OrderLog)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface OrderLogDao extends BaseMapper<OrderLog> {

}

