package com.feri.co.order.listener;

import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.order.dao.OrderDao;
import com.feri.co.order.dao.OrderLogDao;
import com.feri.co.order.entity.Order;
import com.feri.co.order.entity.OrderLog;
import com.feri.co.order.feign.UserCouponService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/14 09:36
 */
@Component
@Slf4j
public class OrderPayListener {
    @Resource
    private OrderDao dao;
    @Resource
    private OrderLogDao logDao;
    @Resource
    private UserCouponService couponService;

    @RabbitListener(queues = RabbitMQConfig.DEAD_QUEUE)
    public void handler(Integer id){
        log.info("延迟获取订单id");
        Order order=dao.selectById(id);
        if(order!=null){
            if(order.getFlag()==5){
                //如果超时了，结果订单还未支付，那么就自动更改订单状态为超时
                order.setFlag(10);
                order.setUtime(new Date());
                if(dao.updateById(order)>0){
                    //状态 变化  记录流水
                    logDao.insert(new OrderLog(order.getId(),order.getFlag(),"订单超过时间未支付，自动取消订单，更改状态位超时！"));
                    //如果用到了 优惠券 需要还回
                    if(order.getUcid()>0){
                        //使用了优惠券
                        couponService.updateFlag(order.getUcid(),1);
                    }
                }
            }
        }
    }
}
