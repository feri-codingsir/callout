package com.feri.co.order.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.OrderLogDao;
import com.feri.co.order.entity.OrderLog;
import com.feri.co.order.service.OrderLogService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * 18.订单流水表(OrderLog)表服务实现类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Service
public class OrderLogServiceImpl implements OrderLogService {
    @Resource
    private OrderLogDao dao;


    @Override
    public R page(PageBo bo) {
        Page<OrderLog> page=new Page<>(bo.getPage(),bo.getSize());
        return R.ok(dao.selectPage(page,null));
    }
}

