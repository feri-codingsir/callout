package com.feri.co.order.controller;

import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.UserAddressAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.UserAddress;
import com.feri.co.order.service.UserAddressService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
/**
 * 16.用户服务地址(UserAddress)表控制层
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@RestController
@RequestMapping("/server/address/")
public class UserAddressController{
    /**
     * 服务对象
     */
    @Resource
    private UserAddressService service;

    @PostMapping("save")
    public R save(@RequestBody UserAddressAddDto dto, HttpServletRequest request){
        return service.save(dto,request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    /**
     * 修改*/
    @PostMapping("update")
    public R update(@RequestBody UserAddress address){
        return service.update(address);
    }
    /**
     * 查询全部*/
    @GetMapping("all")
    public R all(){
        return service.all();
    }
    /**
     * 查询默认*/
    @GetMapping("default")
    public R defaultOne(HttpServletRequest request){
        return service.defaultOne(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
}
