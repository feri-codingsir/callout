package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.order.entity.UserAddress;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 16.用户服务地址(UserAddress)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface UserAddressDao extends BaseMapper<UserAddress> {

    @Update("update t_user_address set flag=2,utime=now() where uid=#{uid} and flag=1 and id !=#{id}")
    int updateFlag(@Param("uid") String uid,@Param("id") int id);
}

