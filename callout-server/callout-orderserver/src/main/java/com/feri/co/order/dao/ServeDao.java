package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.common.dto.ServeDto;
import com.feri.co.order.entity.Serve;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 13.服务表(Serve)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-11 11:46:14
 */
public interface ServeDao extends BaseMapper<Serve> {

    @Select("select s.*,st.name stname,sd.id sdid,sd.name,sd.price,sd.curr_price,sd.specs from t_serve s \n" +
            "inner join t_serve_detail sd on s.id=sd.sid\n" +
            "inner join t_serve_type st on s.stid=st.id\n" +
            "where s.id=#{id}")
    ServeDto selectById(int id);

    @Select("select s.*,st.name stname,sd.id sdid,sd.name,sd.price,sd.curr_price,sd.specs from t_serve s \n" +
            "inner join t_serve_detail sd on s.id=sd.sid\n" +
            "inner join t_serve_type st on s.stid=st.id\n" +
            "where sd.id=#{sdid}")
    ServeDto selectBySdId(int sdid);

    @Select("select s.*,st.name stname,sd.id sdid,sd.name,sd.price,sd.curr_price,sd.specs from t_serve s \n" +
            "inner join t_serve_detail sd on s.id=sd.sid\n" +
            "inner join t_serve_type st on s.stid=st.id\n" +
            "inner join t_cart c on c.sdid=sd.id\n" +
            "where c.id=#{cid}")
    ServeDto selectByCid(int cid);
    @Select("select s.*,st.name stname,sd.id sdid,sd.name,sd.price,sd.curr_price,sd.specs from t_serve s \n" +
            "inner join t_serve_detail sd on s.id=sd.sid\n" +
            "inner join t_serve_type st on s.stid=st.id\n" +
            "where st.parent_id=#{id} order by sd.curr_price asc limit 10")
    List<ServeDto> selectTop(int id);

    @Select("select s.*,st.name stname,sd.id sdid,sd.name,sd.price,sd.curr_price,sd.specs from t_serve s \n" +
            "inner join t_serve_detail sd on s.id=sd.sid\n" +
            "inner join t_serve_type st on s.stid=st.id\n" +
            "where ${type} order by ${order} asc ")
    List<ServeDto> selectList(@Param("type") String type,@Param("order") String order);
}

