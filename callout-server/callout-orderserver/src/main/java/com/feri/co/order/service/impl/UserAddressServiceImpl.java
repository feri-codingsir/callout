package com.feri.co.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.UserAddressAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.UserAddressDao;
import com.feri.co.order.entity.UserAddress;
import com.feri.co.order.service.UserAddressService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;

/**
 * 16.用户服务地址(UserAddress)表服务实现类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Service
public class UserAddressServiceImpl implements UserAddressService {
    @Resource
    private UserAddressDao dao;
    @Resource
    private StringRedisTemplate template;

    @Override
    public R save(UserAddressAddDto dto, String token) {
        //1.获取登录用户
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        //2.构建对象
        UserAddress address=new UserAddress();
        BeanUtils.copyProperties(dto,address);
        address.setCtime(new Date());
        address.setUtime(new Date());
        address.setUid(Integer.parseInt(uid));
        //3.操作数据库
        if(dao.insert(address)>0){
            //4.如果服务地址为默认的，那么其他的就不能为默认的了
            if(address.getFlag()==1){
                //修改标记位
                dao.updateFlag(uid,address.getId());
            }
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R update(UserAddress address) {
        address.setUtime(new Date());
        if(dao.updateById(address)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R all() {
        return R.ok(dao.selectList(null));
    }

    @Override
    public R defaultOne(String token) {
        LambdaQueryWrapper<UserAddress> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserAddress::getFlag,1).eq(UserAddress::getUid,template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token));
        return R.ok(dao.selectOne(wrapper));
    }
}

