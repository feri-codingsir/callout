package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.order.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 17.订单表(Order)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface OrderDao extends BaseMapper<Order> {

    @Update("update t_order set flag=#{flag} where no=#{no}")
    int updateFlag(@Param("no") String no,@Param("flag") int flag);
}

