package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 18.订单流水表(OrderLog)表实体类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Data
@NoArgsConstructor
@TableName("t_order_log")
public class OrderLog{
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //订单id
    private Integer oid;
    //字典表-订单状态
    private Integer type;
    //备注信息
    private String info;
    //创建时间
    private Date ctime;

    public OrderLog(Integer oid, Integer type, String info) {
        this.oid = oid;
        this.type = type;
        this.info = info;
        this.ctime=new Date();
    }
}
