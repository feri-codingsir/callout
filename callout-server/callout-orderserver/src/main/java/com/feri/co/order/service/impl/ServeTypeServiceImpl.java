package com.feri.co.order.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.ServeTypeAddDto;
import com.feri.co.common.dto.ServeTypeDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.ServeTypeDao;
import com.feri.co.order.entity.Serve;
import com.feri.co.order.entity.ServeType;
import com.feri.co.order.service.ServeTypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 12.服务类型表(ServeType)表服务实现类
 *
 * @author Feri
 * @since 2023-08-11 11:46:17
 */
@Service
public class ServeTypeServiceImpl implements ServeTypeService {
    @Resource
    private ServeTypeDao dao;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    @Override
    public R save(ServeTypeAddDto dto) {
        //校验
        if(dto!=null){
            ServeType type=new ServeType();
            BeanUtils.copyProperties(dto,type);
            type.setCtime(new Date());
            //新增数据
            if(dao.insert(type)>0){
                //缓存
                redisTemplate.delete(RedisKeyConfig.SERVE_TYPES);
                return R.ok();
            }
        }
        return R.fail();
    }

    @Override
    public R update(ServeType type) {
        if(dao.updateById(type)>0){
            //缓存
            redisTemplate.delete(RedisKeyConfig.SERVE_TYPES);
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R del(int id) {
        if(dao.deleteById(id)>0){
            //缓存
            redisTemplate.delete(RedisKeyConfig.SERVE_TYPES);
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R queryHome() {
        //集合 一级 每个一级对象内部又一个集合 所属二级
        if(redisTemplate.hasKey(RedisKeyConfig.SERVE_TYPES)){
            return R.ok(redisTemplate.opsForList().range(RedisKeyConfig.SERVE_TYPES,0,redisTemplate.opsForList().size(RedisKeyConfig.SERVE_TYPES)));
        }else {
            //缓存不存在 先请求数据库 更新Redis 最后返回结果
            List<ServeTypeDto> list=dao.selectHome();
            redisTemplate.opsForList().leftPushAll(RedisKeyConfig.SERVE_TYPES,list);
            return R.ok(list);
        }
    }

    @Override
    public R queryPage(PageBo bo) {
        Page<ServeType> page=new Page<>(bo.getPage(),bo.getSize());
        return R.ok(dao.selectPage(page,null));
    }
}

