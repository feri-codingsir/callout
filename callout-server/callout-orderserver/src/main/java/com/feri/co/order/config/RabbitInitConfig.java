package com.feri.co.order.config;

import com.feri.co.common.config.RabbitMQConfig;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/14 09:29
 */
@Configuration
public class RabbitInitConfig {
    @Bean
    public Queue createQttl(){
        Map<String,Object> param=new HashMap<>();
//        param.put("x-message-ttl",10*60*1000);
        //为了测试 改为10秒
        param.put("x-message-ttl",10*1000);
        param.put("x-deat-leater-exchange", RabbitMQConfig.DEAD_EXCHANGE);
        param.put("","orderto");
        return QueueBuilder.durable(RabbitMQConfig.TTL_QUEUE).withArguments(param).build();
    }
    @Bean
    public Queue createQDead(){
        return new Queue(RabbitMQConfig.DEAD_QUEUE);
    }
    @Bean
    public Queue createQRefound(){
        return new Queue(RabbitMQConfig.ORDER_R_QUEUE);
    }
    @Bean
    public DirectExchange createDe(){
        return new DirectExchange(RabbitMQConfig.DEAD_EXCHANGE);
    }
    @Bean
    public Binding createBd(DirectExchange de){
        return BindingBuilder.bind(createQDead()).to(de).with("orderto");
    }
}
