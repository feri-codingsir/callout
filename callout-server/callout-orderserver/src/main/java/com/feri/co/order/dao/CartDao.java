package com.feri.co.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.common.dto.CartDetailDto;
import com.feri.co.order.entity.Cart;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 15.购物车表(Cart)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface CartDao extends BaseMapper<Cart> {

    @Select("update t_cart set num=#{num} where id=#{id}")
    int updateNum(@Param("id") int id,@Param("num") int num);


    List<CartDetailDto> selectByUid(int uid);
}

