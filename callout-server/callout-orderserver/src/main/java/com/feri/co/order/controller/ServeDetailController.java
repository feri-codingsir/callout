package com.feri.co.order.controller;

import com.feri.co.common.dto.ServeDetailAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.ServeDetail;
import com.feri.co.order.service.ServeDetailService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * 14.服务详情表(ServeDetail)表控制层
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@RestController
@RequestMapping("/server/servedetail/")
public class ServeDetailController{
    /**
     * 服务对象
     */
    @Resource
    private ServeDetailService service;
    /**
     * 新增*/
    @PostMapping("save")
    public R save(@RequestBody ServeDetailAddDto dto){
        return service.save(dto);
    }
    /**
     * 修改*/
    @PostMapping("update")
    public R update(@RequestBody ServeDetail detail){
        return service.update(detail);
    }
    /**
     * 上下架*/
    @GetMapping("updown")
    public R updateFlag(int id,int flag){
        return service.updateFlag(id, flag);
    }
    /**
     * 删除 假删除 逻辑删除 修改标记位*/
    @GetMapping("del")
    public R del(int id){
        return service.del(id);
    }
}
