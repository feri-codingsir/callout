package com.feri.co.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.CartAddDto;
import com.feri.co.common.dto.CartUpdateNumDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.dao.CartDao;
import com.feri.co.order.entity.Cart;
import com.feri.co.order.service.CartService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 15.购物车表(Cart)表服务实现类
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Service
public class CartServiceImpl implements CartService {
    @Resource
    private CartDao dao;
    @Resource
    private StringRedisTemplate template;

    @Override
    public R save(CartAddDto dto, String token) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        if(dto!=null){
            //查询之前是否存在
            LambdaQueryWrapper<Cart> wrapper=new LambdaQueryWrapper<>();
            wrapper.eq(Cart::getUid,uid).eq(Cart::getSdid,dto.getSdid());
            Cart cart=dao.selectOne(wrapper);
            if(cart==null){
                //第一次 新增
                cart=new Cart();
                BeanUtils.copyProperties(dto,cart);
                cart.setCtime(new Date());
                cart.setUtime(new Date());
                if(dao.insert(cart)>0){
                    return R.ok();
                }
            }else {
                //之前添加过 修改数量
                cart.setNum(cart.getNum()+dto.getNum());
                cart.setUtime(new Date());
                if(dao.updateById(cart)>0){
                    return R.ok();
                }
            }
        }
        return R.fail();
    }


    @Override
    public R updateNum(CartUpdateNumDto dto) {
        if(dto!=null){
            //修改 会不会出现频繁点击 数量变化？如果出现如何解决？
            //操作到Redis 为啥？存储具体数据?有效期？数据类型？数据同步？ 思考
            if(dao.updateNum(dto.getId(),dto.getNum())>0){
                return R.ok();
            }
        }
        return R.fail();
    }

    @Override
    public R queryByUid(String token) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);

        return R.ok(dao.selectByUid(Integer.parseInt(uid)));
    }

    @Override
    public R del(int id) {
        if(id>0){
            if(dao.deleteById(id)>0){
                return R.ok();
            }
        }
        return R.fail();
    }
}

