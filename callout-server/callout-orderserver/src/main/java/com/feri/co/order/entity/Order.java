package com.feri.co.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 17.订单表(Order)表实体类
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@Data
@TableName("t_order")
public class Order {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //订单标题
    private String title;
    //订单金额
    private Double money;
    //支付金额
    private Double payMoney;
    //订单备注
    private String info;
    //用户服务地址
    private Integer uaid;
    //服务产品id
    private Integer sdid;
    //技术id 针对上门按摩
    private Integer tid;
    //服务时间
    private Date sTime;
    //用户优惠券
    private Integer ucid;
    //字典表-支付方式支付宝 、微信 、银联
    private Integer payType;
    //订单号
    private String no;
    //字典表-订单状态：未支付 、已支付，未服务、已服务，未评价 、已评价 、取消订单、支付超时 、退款
    private Integer flag;
    //创建时间
    private Date ctime;
    //更新时间
    private Date utime;
    //服务数量
    private Integer num;
    //服务产品的图片
    private String simg;
    //服务产品价格
    private Double sprice;
    //服务产品规格
    private String specs;
}
