package com.feri.co.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.dto.ServeTypeAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.ServeType;

/**
 * 12.服务类型表(ServeType)表服务接口
 *
 * @author Feri
 * @since 2023-08-11 11:46:17
 */
public interface ServeTypeService{

    /**
     * 新增*/
    R save(ServeTypeAddDto dto);
    /**
     * 修改
     **/
    R update(ServeType type);
    /**
     * 删除
     **/
    R del(int id);
    /**
     * 查询 首页 一级 和二级*/
    R queryHome();
    /**
     * 查询所有 分页 */
    R queryPage(PageBo bo);
}

