package com.feri.co.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.OrderLog;

/**
 * 18.订单流水表(OrderLog)表服务接口
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface OrderLogService{

    /**
     * 查询*/
    R page(PageBo bo);

}

