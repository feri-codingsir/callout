package com.feri.co.order.service;

import com.feri.co.common.dto.OrderAddDto;
import com.feri.co.common.vo.R;

/**
 * 17.订单表(Order)表服务接口
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
public interface OrderService{
    /**
     * 购买-下单*/
    R buy(OrderAddDto dto,String token);
    /**
     * 修改状态*/
    R update(String no,int flag);
    /**
     * 查询某个用户的订单 根据订单状态*/
    R queryByUid(String token);
    /**
     * 输入参数：要么服务id要么是购物车id
     * @param type 1详情下单 2.购物车下单
     * 订单预览返回的数据
     * 1.服务地址（默认地址）
     * 2.服务详情 规格
     * 3.优惠券信息
     * 4.vip信息 95折
     * 5.订单备注信息、选择支付方式*/
    R queryPre(int type,int id,String token);
    /**
     * 取消订单
     * 未支付*/
    R cancelOrder(String no);
    /**
     * 退款订单
     * 已支付
     * 小心规则：服务前2小时内退单将按30元/人扣空单费*/
    R refoundOrder(String no,String reason);
}

