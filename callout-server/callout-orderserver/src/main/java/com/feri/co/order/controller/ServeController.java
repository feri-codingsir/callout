package com.feri.co.order.controller;

import com.feri.co.common.dto.ServeAddDto;
import com.feri.co.common.vo.R;
import com.feri.co.order.entity.Serve;
import com.feri.co.order.service.ServeService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
/**
 * 13.服务表(Serve)表控制层
 *
 * @author Feri
 * @since 2023-08-11 11:46:13
 */
@RestController
@RequestMapping("/server/serve/")
public class ServeController{
    /**
     * 服务对象
     */
    @Resource
    private ServeService service;

    /**
     * 新增
     **/
    @PostMapping("save")
    public R save(@RequestBody ServeAddDto dto){
        return service.save(dto);
    }
    /**
     * 修改
     **/
    @PostMapping("update")
    public R update(@RequestBody Serve serve){
        return service.update(serve);
    }
    /**
     * 删除
     **/
    @GetMapping("del")
    public R del(int id){
        return service.del(id);
    }
    /**
     * 查询 首页榜单*/
    @GetMapping("top")
    public R queryTop(int stid){
        return service.queryTop(stid);
    }
    /**
     * 查询 根据类型 要是动态排序 分页*/
    @GetMapping("list")
    public R queryList(int level,int type,String order){
        return service.queryList(level, type, order);
    }
    /**
     * 查询 详情 热门服务 缓存*/
    @GetMapping("detail")
    public R queryDetail(int id){
        return service.queryDetail(id);
    }

}

