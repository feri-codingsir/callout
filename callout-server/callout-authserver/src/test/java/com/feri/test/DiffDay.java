package com.feri.test;

import cn.hutool.core.date.LocalDateTimeUtil;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/10 16:49
 */
public class DiffDay {
    @Test
    public void t1(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        LocalDateTime start = LocalDateTimeUtil.parse(sdf.format(new Date())+"T00:00:00");
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.HOUR,9);
        LocalDateTime end = LocalDateTimeUtil.parse(sdf.format(calendar.getTime())+"T00:00:00");

        System.err.println(LocalDateTimeUtil.between(start, end).toDays());
    }
}
