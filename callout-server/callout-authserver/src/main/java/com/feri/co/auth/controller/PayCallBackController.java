package com.feri.co.auth.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.feri.co.auth.entity.PayLog;
import com.feri.co.auth.pay.AliPayUtil;
import com.feri.co.auth.service.PayLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/9 11:38
 */
@Slf4j
@Controller
public class PayCallBackController {

    @Resource
    private PayLogService service;
    @PostMapping("/api/co/alipaycb")
    public void alipayCall(HttpServletRequest request, HttpServletResponse response) throws AlipayApiException, IOException {
        log.info("支付宝支付回调开始");
        //1.验证签名 确定就是支付宝发起的请求
        Map<String,String[]> map=request.getParameterMap();
        System.err.println(map);
        //将异步通知中收到的所有参数都存放到map中
        Map<String, String> paramsMap = new HashMap<>();
        map.keySet().forEach(k->{
            if(!k.equals("sign") && !k.equals("sign_type")){
                paramsMap.put(k,map.get(k)[0]);
            }
        });
        System.err.println(paramsMap.keySet());
//        boolean signVerified = AlipaySignature.rsaCheckV1(paramsMap, AliPayUtil.ALIPAY_PUBLIC_KEY, "UTF-8", "RSA2"); //调用SDK验证签名
//        if(signVerified){
//            // TODO 验签成功后，按照支付结果异步通知中的描述，对支付结果中的业务内容进行二次校验，校验成功后在response中返回success并继续商户自身业务处理，校验失败返回failure
//            //说明支付成功
//            log.info("支付宝支付成功，订单号：{},支付金额：{}",paramsMap.get("out_trade_no"),paramsMap.get("total_amount"));
//        }else{
//            // TODO 验签失败则记录异常日志，并在response中返回failure.
//            response.getWriter().println("failure");
//        }

        //获取订单号
        String no=request.getParameter("out_trade_no");
        //查询支付记录
        if(service.updateFlag(no,2)>0) {
            response.getWriter().println("success");
        }else {
            response.getWriter().println("failure");
        }
    }
}
