package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 8.支付记录表(PayLog)表实体类
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@Data
@TableName("t_pay_log")
public class PayLog extends Model<PayLog> {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //支付类型 1支付宝 2微信 3银联
    private Integer payType;
    //订单号
    private String no;
    //支付金额，单位 元
    private Double payMoney;
    //标记位 1.未支付 2已支付 3退款
    private Integer flag;
    //创建时间
    private Date ctime;
    //更新时间
    private Date utime;
    //支付标题
    private String title;
    //用户id
    private Integer uid;
    //操作类型：1.VIP充值 2.钱包充值 3服务订单支付
    private Integer type;
}
