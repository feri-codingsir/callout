package com.feri.co.auth.controller;

import com.feri.co.auth.service.UserWalletService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 6.用户钱包表(UserWallet)表控制层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@RestController
@RequestMapping("/server/userwallet/")
public class UserWalletController{
    /**
     * 服务对象
     */
    @Resource
    private UserWalletService service;

    @GetMapping("my")
    public R my(HttpServletRequest request){
        return service.queryByToken(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    @GetMapping("money")
    public double money(String uid){
        return service.queryMoney(uid);
    }
    @GetMapping("updatemoney")
    public int updateMoney(double money,int uid){
        return service.updateMoney(uid,money);
    }
}

