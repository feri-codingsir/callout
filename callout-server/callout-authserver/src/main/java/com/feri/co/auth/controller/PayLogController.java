package com.feri.co.auth.controller;

import com.feri.co.auth.service.PayLogService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.PayAddDto;
import com.feri.co.common.dto.PayDto;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 8.支付记录表(PayLog)表控制层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@RestController
@RequestMapping("/server/pay/")
public class PayLogController{
    /**
     * 服务对象
     */
    @Resource
    private PayLogService service;

    @PostMapping("save")
    public R save(@RequestBody PayDto dto, HttpServletRequest request){
        return service.save(dto,request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    @GetMapping("all")
    public R all(HttpServletRequest request){
        return service.queryUid(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    @PostMapping("add")
    public int add(@RequestBody PayAddDto dto){
        return service.add(dto);
    }
}

