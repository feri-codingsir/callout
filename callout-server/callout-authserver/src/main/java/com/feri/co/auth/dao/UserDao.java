package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 1.用户表(User)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
public interface UserDao extends BaseMapper<User> {

    @Update("update t_user set password=#{pass},utime=now() where phone=#{phone})")
    int updatePass(@Param("phone") String phone,@Param("pass") String pass);

    @Select("select id from t_user where flag=1")
    List<Integer> selectIds();
}

