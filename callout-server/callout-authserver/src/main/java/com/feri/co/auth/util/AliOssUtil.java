package com.feri.co.auth.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import java.io.ByteArrayInputStream;
import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/5/5 14:51
 */
public class AliOssUtil {
    // yourEndpoint填写Bucket所在地域对应的Endpoint
    private static final String endpoint = "https://oss-cn-hangzhou.aliyuncs.com";
    // 阿里云账号AccessKey
    private static final String accessKeyId = "LTAI5tLEfnQSsgsvPbegUnWM";
    private static final String accessKeySecret = "J6H14aiuycDfYnDQeCsYxy7hqbxy91";
    public static final String BUCKET="javagp202301";
    //客户端
    private static OSS client;
    static {
        // 创建OSSClient实例。
         client = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }

    /**
     * 创建存储空间名称
     * @param name 存储空间名称*/
   public static boolean create(String name){
       // 创建CreateBucketRequest对象。
       CreateBucketRequest createBucketRequest = new CreateBucketRequest(name);
       // 执行操作创建存储空间
       Bucket bucket=client.createBucket(createBucketRequest);
       return bucket!=null;
   }
   /**
    * 实现文件上传
    * @param bucket 存储空间名称
    * @param obj 存储对象名称，带文件后缀
    * @param data 文件内容
    * @return 是否成功*/
   public static boolean upload(String bucket,String obj,byte[] data){
       PutObjectRequest request = new PutObjectRequest(bucket, obj, new ByteArrayInputStream(data));
       request.setProcess("true");
       PutObjectResult result=client.putObject(request);
       return result.getResponse().getStatusCode()==200;
   }
   /**
    * 创建访问链接
    * @param bucket 存储空间名称
    * @param obj 存储对象名称，带文件后缀
    * @param etime 访问地址的失效时间
    * @return 访问地址
    * */
   public static String createUrl(String bucket, String obj, Date etime){
       return client.generatePresignedUrl(bucket,obj,etime).toString();
   }
   /**
    * 删除文件
    * @param bucket 存储空间名称
    * @param obj 存储对象名称，带文件后缀
     */
   public static void delFile(String bucket,String obj){
       client.deleteObject(bucket, obj);
   }

}
