package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.UserWallet;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 6.用户钱包表(UserWallet)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
public interface UserWalletDao extends BaseMapper<UserWallet> {

    @Select("select balance from t_user_wallet where uid=#{uid}")
    double selectMoney(String uid);
    @Update("update t_user_wallet set balance=balance+#{money},utime=now() where uid=#{uid} ")
    int updateMoney(@Param("money") double money,@Param("uid") int uid);
}

