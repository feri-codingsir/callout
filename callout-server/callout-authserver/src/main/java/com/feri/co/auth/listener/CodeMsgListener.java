package com.feri.co.auth.listener;

import com.feri.co.auth.dao.SmsLogDao;
import com.feri.co.auth.entity.SmsLog;
import com.feri.co.auth.util.AliSmsUtil;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.mq.CodeMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/8 14:41
 */
@Slf4j
@Component
public class CodeMsgListener {
    @Resource
    private StringRedisTemplate template;
    @Resource
    private SmsLogDao dao;

    @RabbitListener(queues = RabbitMQConfig.SMS_QUEUE)
    public void hanlder(CodeMsg msg){
        log.info("开始发送短信,{}",System.currentTimeMillis());
        //1.发送短信
        if(AliSmsUtil.sendSms(msg.getTem(),msg.getPhone(),msg.getCode())){
            //2.成功 记录验证码到Redis
            template.opsForValue().set(msg.getKey()+msg.getPhone(),msg.getCode(),10, TimeUnit.MINUTES);
            //3.记录短信发送日志
            dao.insert(new SmsLog(msg.getPhone(),1,"发送短信验证码成功"));
        }
        log.info("结束发送短信,{}",System.currentTimeMillis());
    }
}
