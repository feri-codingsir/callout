package com.feri.co.auth.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.OssLogDao;
import com.feri.co.auth.entity.OssLog;
import com.feri.co.auth.service.AliOssService;
import com.feri.co.auth.util.AliOssUtil;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * 4.对象存储表(OssLog)表服务实现类
 *
 * @author Feri
 * @since 2023-08-09 09:36:13
 */
@Service
public class AliOssServiceImpl implements AliOssService {
    @Resource
    private OssLogDao dao;

    private R upload(MultipartFile file,int type) throws IOException {
        //1.验证上传的内容是否存在
        if(!file.isEmpty()){
            //2.获取上传文件名
            String objName=rename(file.getOriginalFilename());
            //3.访问地址的有效期
            Date time=getYear(3);
            //4.上传文件到阿里云对象存储
            if(AliOssUtil.upload(AliOssUtil.BUCKET,objName,file.getBytes())){
                String url=AliOssUtil.createUrl(AliOssUtil.BUCKET,objName,time);
                //5.记录日志
                dao.insert(new OssLog(AliOssUtil.BUCKET,objName,url,type));
                return R.ok(url);
            }
        }
        return R.fail();
    }
    @Override
    public R uploadImg(MultipartFile file) throws IOException {
        return upload(file,1);
    }

    @Override
    public R uploadVideo(MultipartFile file) throws IOException {
        return upload(file,2);
    }

    @Override
    public R all(PageBo bo) {
        Page<OssLog> page=new Page<>(bo.getPage(), bo.getSize());
        return R.ok(dao.selectPage(page,null));
    }
    /**
     * 对上传的文件名进行重命名 保证唯一性
     * @param name 需要重命名的名称*/
    private String rename(String name){
        //1.长度
        if(name.length()>50){
            name=name.substring(name.length()-50);
        }
        //2.重命名
        return System.currentTimeMillis()+"_"+new Random().nextInt(100000)+"_"+name;
    }
    /**
     * 获取指定年后*/
    private Date getYear(int year){
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.YEAR,year);
        return calendar.getTime();
    }
}

