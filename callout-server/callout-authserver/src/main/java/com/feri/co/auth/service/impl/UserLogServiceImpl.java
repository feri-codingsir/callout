package com.feri.co.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.UserLogDao;
import com.feri.co.auth.entity.UserLog;
import com.feri.co.auth.service.UserLogService;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.vo.R;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 2.用户日志表(UserLog)表服务实现类
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@Service
public class UserLogServiceImpl implements UserLogService {
    @Resource
    private UserLogDao dao;
    @Resource
    private StringRedisTemplate template;

    @Override
    public R queryByUid(String token) {
        LambdaQueryWrapper<UserLog> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserLog::getUid,template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token));
        wrapper.orderByDesc(UserLog::getCtime);
        return R.ok(dao.selectList(wrapper));
    }
}

