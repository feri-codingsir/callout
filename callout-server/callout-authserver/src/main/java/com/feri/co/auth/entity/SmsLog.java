package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 3.短信发送记录(SmsLog)表实体类
 * @author Feri
 * @since 2023-08-08 13:58:32
 */
@Data
@NoArgsConstructor
@TableName("t_sms_log")
public class SmsLog {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //手机号
    private String phone;
    //类型 1.短信验证码 2.通知消息
    private Integer type;
    //内容信息
    private String info;
    //创建时间
    private Date ctime;

    public SmsLog(String phone, Integer type, String info) {
        this.phone = phone;
        this.type = type;
        this.info = info;
        this.ctime=new Date();
    }
}
