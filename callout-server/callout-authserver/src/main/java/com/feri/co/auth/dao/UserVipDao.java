package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.UserVip;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 7.用户vip表(UserVip)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
public interface UserVipDao extends BaseMapper<UserVip> {

    @Select("select uid from t_user_vip where end_date>=#{date}")
    List<Integer> selectUids(String date);
}

