package com.feri.co.auth.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.feri.co.auth.entity.Coupon;
import com.feri.co.auth.service.CouponService;
import com.feri.co.common.dto.CouponAddDto;
import com.feri.co.common.dto.CouponUpdateDto;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 10.优惠券表(Coupon)表控制层
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@RestController
@RequestMapping("/server/coupon/")
public class CouponController {
    /**
     * 服务对象
     */
    @Resource
    private CouponService service;

    @PostMapping("save")
    public R save(@RequestBody CouponAddDto dto){
        return service.save(dto);
    }
    @PostMapping("update")
    public R update(@RequestBody CouponUpdateDto dto){
        return service.update(dto);
    }
    @GetMapping("all")
    public R all(){
        return service.queryList();
    }
}

