package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.SmsLog;
import com.feri.co.common.dto.PhoneCodeDto;
import com.feri.co.common.vo.R;

/**
 * 3.短信发送记录(SmsLog)表服务接口
 *
 * @author Feri
 * @since 2023-08-08 13:58:33
 */

public interface SmsLogService {
    /**
     * 发送注册验证码*/
    R sendRCode(String phone);
    /**
     * 发送密码找回验证码*/
    R sendFCode(String phone);
    /**
     * 发送登录验证码*/
    R sendLCode(String phone);
    /**
     * 校验注册验证码*/
    R checkRCode(PhoneCodeDto dto);
    R all();

}

