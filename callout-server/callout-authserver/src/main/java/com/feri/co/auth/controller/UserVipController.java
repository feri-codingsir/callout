package com.feri.co.auth.controller;

import com.feri.co.auth.service.UserVipService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 7.用户vip表(UserVip)表控制层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@RestController
@RequestMapping("/server/vip/")
public class UserVipController {
    /**
     * 服务对象
     */
    @Resource
    private UserVipService service;

    @GetMapping("vip")
    public R queryUid(HttpServletRequest request){
        return service.queryByUid(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    @GetMapping("checkvip")
    public boolean checkVip(String uid){
        return service.checkVip(uid);
    }
}
