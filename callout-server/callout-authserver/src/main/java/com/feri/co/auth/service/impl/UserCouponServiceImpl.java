package com.feri.co.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.UserCouponDao;
import com.feri.co.auth.dao.UserVipDao;
import com.feri.co.auth.entity.Coupon;
import com.feri.co.auth.entity.UserCoupon;
import com.feri.co.auth.entity.UserVip;
import com.feri.co.auth.service.UserCouponService;
import com.feri.co.auth.util.DateUtil;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.UserCouponDto;
import com.feri.co.common.vo.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 11.用户优惠券表(UserCoupon)表服务实现类
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@Slf4j
@Service
public class UserCouponServiceImpl implements UserCouponService {
    @Resource
    private UserCouponDao dao;
    @Resource
    private UserVipDao vipDao;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private StringRedisTemplate template;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Transactional
    @Override
    public R save(String token, int cid) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token).toString();
        //1.验证缓存是否存在优惠券 是否还可以领取
        if(redisTemplate.opsForHash().hasKey(RedisKeyConfig.COUPONS,cid+"")){
            //2.验证资格
            Coupon coupon= (Coupon) redisTemplate.opsForHash().get(RedisKeyConfig.COUPONS,cid+"");
            if(coupon.getCtype()==2){
                //vip专属
                LambdaQueryWrapper<UserVip> wrapper=new LambdaQueryWrapper<>();
                wrapper.eq(UserVip::getUid,uid);
                wrapper.ge(UserVip::getEndDate, DateUtil.getToday());
                UserVip vip=vipDao.selectOne(wrapper);
                if(vip==null){
                    return R.fail("亲，你没资格！快来充值VIP!");
                }
            }
            //验证优惠券 剩余数量
            if(coupon.getNum()>0){
                //可以领取
                if(dao.insert(new UserCoupon(Integer.parseInt(uid),cid))>0) {
                    coupon.setNum(coupon.getNum() - 1);
                    redisTemplate.opsForHash().put(RedisKeyConfig.COUPONS,cid+"",coupon);

                }
                return R.ok();
            }

        }
        return R.fail();
    }

    @Override
    public R queryMy(String token) {
        Object uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        log.info("uid:{}",uid);
        return R.ok(dao.selectByUid(uid.toString()));
    }

    @Override
    public List<UserCouponDto> queryAccess(String uid) {
        //Object uid=redisTemplate.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);

        return dao.selectAccess(DateUtil.getToday(),uid);
    }

    @Override
    public int updateFlag(int id, int flag) {
        UserCoupon coupon=new UserCoupon();
        coupon.setFlag(flag);
        coupon.setId(id);
        coupon.setUtime(new Date());
        return dao.updateById(coupon);
    }

    @Override
    public UserCouponDto queryByUcid(int id) {
        return dao.selectCoupon(id);
    }
}

