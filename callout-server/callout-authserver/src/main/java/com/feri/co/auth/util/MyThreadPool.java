package com.feri.co.auth.util;

import com.feri.co.auth.dao.UserCouponDao;
import org.springframework.scheduling.concurrent.DefaultManagedAwareThreadFactory;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/10 16:23
 */
public class MyThreadPool {
    public static ThreadPoolExecutor executor;
    static {
        /*
        * 原生线程池
        * 参七个数说明：
        * 1.核心线程数
        * 2.最大线程数
        * 3.线程回收的空闲时间，默认不会回收核心线程
        * 4.时间单位
        * 5.阻塞队列 任务数超过核心线程数 就会存储到阻塞队列
        * 6.线程创建工厂
        * 7.拒绝策略 核心+阻塞+最大 都满了 触发拒绝 直接放弃新任务*/
        executor=new ThreadPoolExecutor(8,64,10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(64),new DefaultManagedAwareThreadFactory(),new ThreadPoolExecutor.AbortPolicy());

        //        executor.allowCoreThreadTimeOut(true);
    }
    public static void addTask(UserCouponDao dao,List<Integer> ids, int cid){
        executor.execute(()->{
            dao.insertBatch(ids,cid);
        });
    }

}
