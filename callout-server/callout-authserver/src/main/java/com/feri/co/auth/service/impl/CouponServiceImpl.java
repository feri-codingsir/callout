package com.feri.co.auth.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.CouponDao;
import com.feri.co.auth.entity.Coupon;
import com.feri.co.auth.service.CouponService;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.CouponAddDto;
import com.feri.co.common.dto.CouponUpdateDto;
import com.feri.co.common.vo.R;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 10.优惠券表(Coupon)表服务实现类
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@Service
public class CouponServiceImpl implements CouponService {
    @Resource
    private CouponDao dao;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private RabbitTemplate template;


    @Override
    public R save(CouponAddDto dto) {
        //1.校验
        if(dto!=null){
            Coupon coupon=new Coupon();
            BeanUtils.copyProperties(dto,coupon);
            coupon.setCtime(new Date());
            coupon.setUtime(new Date());
            coupon.setFlag(1);
            if(dao.insert(coupon)>0){
                return R.ok();
            }
        }
        return R.fail();
    }

    @Override
    public R update(CouponUpdateDto dto) {
        //1.校验
        if(dto!=null){
            //2.修改优惠券的状态
            Coupon coupon=new Coupon();
            coupon.setId(dto.getId());
            coupon.setFlag(dto.getFlag());
            coupon.setUtime(new Date());
            if(dao.updateById(coupon)>0){
                //如果优惠券 审核通过  需要根据情况 进行处理
                if(dto.getFlag()==2){
                    //查询优惠券
                    Coupon coupon1=dao.selectById(dto.getId());
                    //根据优惠券的类型 进行不同的操作
                    if(coupon1.getSendType()==1){
                        //3.自动发放 就需要在用户优惠券表中自动插入本张优惠券（小心 用户量越来越大）
                        //RabbitMQ 异步 解耦
                        template.convertAndSend("", RabbitMQConfig.COUPONS_SEND,coupon1);
                    }else {
                        //4.主动领取 那么需要缓存
                        //Redis 思考：1.为什么使用Redis-快（1.内存 2.底层结构优化 3.resp协议 4.多路复用） 2.key是否设置有效期 3.使用什么数据类型 4.存储什么数据 5.如果实现数据同步
                        //可以是String 每种优惠券都单独缓存 ，页面查询优惠券列表。需要使用scan 实现模糊匹配 查询出所有未失效的优惠券
                        //可以是Hash  field:优惠券id value:优惠券对象 不设置有效期，参考Redis的惰性删除，查询的时候，校验每个优惠券是否结束领取，如果结束，从redis删除优惠券
                        redisTemplate.opsForHash().put(RedisKeyConfig.COUPONS,coupon1.getId().toString(),coupon1);
                    }
                    return R.ok();
                }
            }
        }
        return R.fail();
    }

    @Override
    public R queryList() {
        List<Object> coupons=new ArrayList<>();
        List<Object> list=redisTemplate.opsForHash().values(RedisKeyConfig.COUPONS);
        list.stream().filter(o->{
            Coupon coupon= (Coupon) o;
            if(diffDays(coupon.getEndDate())>=0){
                //有效 需要
                return true;
            }else {
                //不能领取 删除数据
                redisTemplate.opsForHash().delete(RedisKeyConfig.COUPONS,coupon.getId()+"");
                //数据同步
                template.convertAndSend("", RabbitMQConfig.COUPONS_SYNC,coupon);
                return false;
            }
        }).forEach(c->{
            coupons.add(c);
        });
        return R.ok(coupons);
    }
    /**
     * 查询2个日期相差的天数*/
    private long diffDays(Date date){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        LocalDateTime start = LocalDateTimeUtil.parse(sdf.format(new Date())+"T00:00:00");
        Calendar calendar=Calendar.getInstance();
        LocalDateTime end = LocalDateTimeUtil.parse(sdf.format(calendar.getTime())+"T00:00:00");

        return LocalDateTimeUtil.between(start, end).toDays();
    }
}

