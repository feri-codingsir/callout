package com.feri.co.auth.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.feri.co.auth.entity.UserCoupon;
import com.feri.co.auth.service.UserCouponService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.UserCouponDto;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * 11.用户优惠券表(UserCoupon)表控制层
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@RestController
@RequestMapping("/server/usercoupon/")
public class UserCouponController{
    /**
     * 服务对象
     */
    @Resource
    private UserCouponService service;

    @GetMapping("add")
    public R add(int cid, HttpServletRequest request){
        return service.save(request.getHeader(SystemConfig.HEADER_TOKEN),cid);
    }
    @GetMapping("my")
    public R myCoupons(HttpServletRequest request){
        return service.queryMy(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    @GetMapping("access")
    public List<UserCouponDto> access(String uid){
        return service.queryAccess(uid);
    }
    @GetMapping("coupon")
    public UserCouponDto coupon(int id){
        return service.queryByUcid(id);
    }
    @GetMapping("updateflag")
    public int updateFlag(int id,int flag){
        return service.updateFlag(id, flag);
    }

}

