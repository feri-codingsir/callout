package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 6.用户钱包表(UserWallet)表实体类
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@Data
@TableName("t_user_wallet")
@NoArgsConstructor
public class UserWallet{
    //序号
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //余额，单位元
    private Double balance;
    //收入，可提现，单位元
    private Double income;
    //总余额，单位元
    private Double totalBalance;
    //总收入，单位元
    private Double totalIncome;
    //创建时间
    private Date ctime;
    //更新时间
    private Date utime;

    public UserWallet(Integer uid, Double balance) {
        this.uid = uid;
        this.balance = balance;
        this.totalBalance = balance;
        this.income=0.0;
        this.totalIncome=0.0;
        this.ctime=new Date();
        this.utime=new Date();
    }
}
