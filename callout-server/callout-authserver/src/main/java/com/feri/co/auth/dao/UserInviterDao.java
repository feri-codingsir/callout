package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.UserInviter;
import com.feri.co.common.dto.UserInviterDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/9 10:14
 */
public interface UserInviterDao extends BaseMapper<UserInviter> {
    @Select("select u.*,ui.ctime uitime from t_user_inviter ui inner join t_user u on ui.uid=u.id where ui.uiid=#{uiid} order by ui.ctime desc")
    List<UserInviterDto> selectAll(int uiid);
}
