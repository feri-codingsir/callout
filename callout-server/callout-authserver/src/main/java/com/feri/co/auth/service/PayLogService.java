package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.PayLog;
import com.feri.co.common.dto.PayAddDto;
import com.feri.co.common.dto.PayDto;
import com.feri.co.common.vo.R;

/**
 * 8.支付记录表(PayLog)表服务接口
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
public interface PayLogService{

    /**
     * 新增支付*/
    R save(PayDto dto,String token);
    /**服务订单的支付*/
    int add(PayAddDto dto);
    /**
     * 支付成功 更改状态*/
    int updateFlag(String no,int flag);
    /**
     * 查询用户充值记录*/
    R queryUid(String token);
}

