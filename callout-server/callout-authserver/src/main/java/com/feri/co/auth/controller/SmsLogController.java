package com.feri.co.auth.controller;

import com.feri.co.auth.service.SmsLogService;
import com.feri.co.common.dto.PhoneCodeDto;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * 3.短信发送记录(SmsLog)表控制层
 *
 * @author Feri
 * @since 2023-08-08 13:58:29
 */
@RestController
@RequestMapping("/server/sms/")
public class SmsLogController{
    /**
     * 服务对象
     */
    @Resource
    private SmsLogService service;

    @PostMapping("checkrcode")
    public R checkRCode(@RequestBody PhoneCodeDto dto){
        return service.checkRCode(dto);
    }
    @PostMapping("sendrcode")
    public R sendRCode(String phone){
        return service.sendRCode(phone);
    }
    @PostMapping("sendlcode")
    public R sendLCode(String phone){
        return service.sendLCode(phone);
    }
    @PostMapping("sendfcode")
    public R sendFCode(String phone){
        return service.sendFCode(phone);
    }
    @GetMapping("all")
    public R all(){
        return service.all();
    }
}

