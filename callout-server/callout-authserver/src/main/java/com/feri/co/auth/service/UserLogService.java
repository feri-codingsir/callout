package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.UserLog;
import com.feri.co.common.vo.R;

/**
 * 2.用户日志表(UserLog)表服务接口
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
public interface UserLogService{
    /**
     * 查询指定用户的日志*/
    R queryByUid(String token);
}

