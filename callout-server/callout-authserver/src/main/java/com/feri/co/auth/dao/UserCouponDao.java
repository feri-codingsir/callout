package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.UserCoupon;
import com.feri.co.common.dto.UserCouponDetail;
import com.feri.co.common.dto.UserCouponDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.awt.*;
import java.util.List;

/**
 * 11.用户优惠券表(UserCoupon)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
public interface UserCouponDao extends BaseMapper<UserCoupon> {

    int insertBatch(@Param("list") List<Integer> uids,@Param("cid") int cid);

    @Select("select c.*,uc.ctime uctime from t_user_coupon uc inner join t_coupon c on uc.cid=c.id where uc.uid=#{uid}")
    List<UserCouponDetail> selectByUid(String uid);

    List<UserCouponDto> selectAccess(@Param("date") String date, @Param("uid") Object uid);

    UserCouponDto selectCoupon(int id);
}

