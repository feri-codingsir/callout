package com.feri.co.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.PayLogDao;
import com.feri.co.auth.dao.UserLogDao;
import com.feri.co.auth.entity.PayLog;
import com.feri.co.auth.entity.UserLog;
import com.feri.co.auth.pay.AliPayUtil;
import com.feri.co.auth.service.PayLogService;
import com.feri.co.auth.service.UserVipService;
import com.feri.co.auth.service.UserWalletService;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.PayAddDto;
import com.feri.co.common.dto.PayDto;
import com.feri.co.common.vo.R;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 8.支付记录表(PayLog)表服务实现类
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@Service
public class PayLogServiceImpl implements PayLogService {
    @Resource
    private PayLogDao dao;
    @Resource
    private UserVipService vipService;
    @Resource
    private StringRedisTemplate template;
    @Resource
    private UserLogDao logDao;
    @Resource
    private UserWalletService walletService;

    @Override
    public R save(PayDto dto, String token) {
        //1.校验
        if(dto!=null){
            PayLog log=new PayLog();
            BeanUtils.copyProperties(dto,log);
            log.setCtime(new Date());
            log.setUtime(new Date());
            log.setFlag(1);
            String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
            log.setNo(System.currentTimeMillis()+"-"+uid);
            log.setUid(Integer.parseInt(uid));
            String payurl="";
            switch (dto.getPayType()){
                case 1://支付宝
                    payurl= AliPayUtil.createPay(log.getNo(),log.getTitle(),log.getPayMoney());
                    break;
                case 2://微信
                    break;
                case 3://银联
                    break;
            }
            if(StringUtils.hasLength(payurl)){
                if(dao.insert(log)>0){
                    return R.ok(payurl);
                }
            }
        }
        return R.fail();
    }

    @Override
    public int add(PayAddDto dto) {
        PayLog log=new PayLog();
        log.setCtime(new Date());
        log.setUtime(new Date());
        log.setFlag(1);
        log.setNo(dto.getNo());
        log.setPayMoney(dto.getPayMoney());
        log.setType(3);
        log.setUid(Integer.parseInt(dto.getUid()));
        return dao.insert(log);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateFlag(String no,int flag) {
        if(dao.updateFlag(no, flag)>0){
            //支付更新成功
            if(flag==2){
                //支付成功了，需要根据充值的内容 去实现业务
                LambdaQueryWrapper<PayLog> wrapper=new LambdaQueryWrapper<>();
                wrapper.eq(PayLog::getNo,no);
                PayLog log=dao.selectOne(wrapper);
                switch (log.getType()){
                    case 1://VIP充值
                        vipService.save(log.getUid());
                        logDao.insert(new UserLog(log.getUid(),4,"用户充值1年的VIP成功！"));
                        break;
                    case 2://钱包充值
                        walletService.add(log.getUid(),log.getPayMoney());
                        break;
                }
            }
            return 1;
        }
        return 0;
    }

    @Override
    public R queryUid(String token) {
        LambdaQueryWrapper<PayLog> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(PayLog::getUid,template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token));
        wrapper.orderByDesc(PayLog::getCtime);
        return R.ok(dao.selectList(wrapper));
    }
}

