package com.feri.co.auth.controller;

import com.feri.co.auth.service.UserService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.FindUserDto;
import com.feri.co.common.dto.LoginDto;
import com.feri.co.common.dto.UserAddDto;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 1.用户表(User)表控制层
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@RestController
@RequestMapping("/server/user/")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService service;

    @GetMapping("checkname")
    public R checkName(String name){
        return service.checkName(name);
    }
    @GetMapping("checkphone")
    public R checkPhone(String phone){
        return service.checkPhone(phone);
    }
    @PostMapping("logincode")
    public R loginCode(@RequestBody LoginDto dto){
        return service.loginCode(dto);
    }
    @PostMapping("loginpass")
    public R loginPass(@RequestBody LoginDto dto){
        return service.loginPass(dto);
    }
    @PostMapping("register")
    public R register(@RequestBody UserAddDto dto){
        return service.register(dto);
    }
    @PostMapping("findpass")
    public R findPass(@RequestBody FindUserDto dto){
        return service.findPass(dto);
    }
    @GetMapping("updatepass")
    public R updatePass(HttpServletRequest request,String pass){
        return service.updatePass(request.getHeader(SystemConfig.HEADER_TOKEN),pass);
    }

    @GetMapping("logout")
    public R logout(HttpServletRequest request){
        return service.logout(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
    @GetMapping("all")
    public R all(){
        return service.all();
    }
    @GetMapping("updateimg")
    public R updateImg(HttpServletRequest request,String url){
        return service.updateImg(request.getHeader(SystemConfig.HEADER_TOKEN),url);
    }
    @GetMapping("inviters")
    public R inviters(HttpServletRequest request){
        return service.queryInviters(request.getHeader(SystemConfig.HEADER_TOKEN));
    }
}
