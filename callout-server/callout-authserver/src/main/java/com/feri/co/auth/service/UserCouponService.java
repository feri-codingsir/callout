package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.UserCoupon;
import com.feri.co.common.dto.UserCouponDto;
import com.feri.co.common.vo.R;

import java.util.List;

/**
 * 11.用户优惠券表(UserCoupon)表服务接口
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
public interface UserCouponService{

    /**
     * 领取优惠券*/
    R save(String token,int cid);
    /**
     * 查询我的优惠券*/
    R queryMy(String token);

    /**
     * 查询有效的 优惠券*/
    List<UserCouponDto> queryAccess(String token);

    /**
     * 修改用户优惠券的状态*/
    int updateFlag(int id,int flag);

    /**
     * 查询指定的优惠券*/
    UserCouponDto queryByUcid(int id);
}

