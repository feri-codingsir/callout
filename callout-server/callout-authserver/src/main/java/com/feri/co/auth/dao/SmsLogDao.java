package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.SmsLog;

/**
 * 3.短信发送记录(SmsLog)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-08 13:58:30
 */
public interface SmsLogDao extends BaseMapper<SmsLog> {

}

