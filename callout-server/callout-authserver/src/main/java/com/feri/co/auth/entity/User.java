package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 1.用户表(User)表实体类
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@Data
@TableName("t_user")
public class User{
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //手机号
    private String phone;
    //昵称
    private String nickname;
    //密码
    private String password;
    //邀请码
    private String inviteCode;
    //标记位：1有效 2无效
    private Integer flag;
    //头像地址
    private String imgurl;
    //更新时间
    private Date utime;
    //创建时间
    private Date ctime;
    public void init(){
        this.ctime=new Date();
        this.utime=new Date();
        this.flag=1;
        this.imgurl="";
    }
}
