package com.feri.co.auth.listener;

import com.feri.co.auth.pay.AliPayUtil;
import com.feri.co.auth.pay.BankPayUtil;
import com.feri.co.auth.pay.WxPayUtil;
import com.feri.co.auth.service.PayLogService;
import com.feri.co.auth.service.UserWalletService;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.dto.OrderRefoundDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/14 11:17
 */
@Component
@Slf4j
public class OrderRefListener {
    @Resource
    private UserWalletService walletService;
    @Resource
    private PayLogService payLogService;


    @RabbitListener(queues = RabbitMQConfig.ORDER_R_QUEUE)
    public void handler(OrderRefoundDto dto){
        log.info("开始处理订单退款的申请");
        switch (dto.getPayType()){
            case 1://支付宝
                if(AliPayUtil.refoundPay(dto.getTitle(),dto.getPayMoney())){
                    payLogService.updateFlag(dto.getNo(),3);
                }
                break;
            case 2://微信
                if(WxPayUtil.refoundPay(dto.getTitle(),dto.getPayMoney())){
                    payLogService.updateFlag(dto.getNo(),3);
                }
                break;

            case 3://支付宝
                if(BankPayUtil.refoundPay(dto.getTitle(),dto.getPayMoney())){
                    payLogService.updateFlag(dto.getNo(),3);
                }
                break;

            case 4://钱包
                walletService.add(dto.getUid(),dto.getPayMoney());
                break;
        }
        log.info("结束处理订单退款的申请");
    }
}
