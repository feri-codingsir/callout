package com.feri.co.auth.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.UserVipDao;
import com.feri.co.auth.entity.UserVip;
import com.feri.co.auth.service.UserVipService;
import com.feri.co.auth.util.DateUtil;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * 7.用户vip表(UserVip)表服务实现类
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@Service
public class UserVipServiceImpl implements UserVipService {
    @Resource
    private UserVipDao dao;
    @Resource
    private StringRedisTemplate template;


    @Override
    public int save(int uid) {
        //1.查询是否存在VIP记录
        LambdaQueryWrapper<UserVip> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserVip::getUid,uid);
        UserVip vip=dao.selectOne(wrapper);
        Calendar calendar=Calendar.getInstance();
        if(vip==null){
            //开通就可以  新增
            calendar.add(Calendar.DAY_OF_MONTH, SystemConfig.VIP_DAYS);
            vip=new UserVip(uid,new Date(),calendar.getTime(),SystemConfig.VIP_DAYS);
            return dao.insert(vip);
        }else {
            //又分为 到期和未到期
            //未到期 直接更新结束日期

            if(diffDay(vip.getEndDate())>=0){
                calendar.setTime(vip.getEndDate());
                calendar.add(Calendar.DAY_OF_MONTH,SystemConfig.VIP_DAYS);
                //更改结束日期
                vip.setEndDate(calendar.getTime());
            }else {
                //到期 更改开始和结束日期
                //更改开始日期
                vip.setStartDate(new Date());
                calendar.add(Calendar.DAY_OF_MONTH,SystemConfig.VIP_DAYS);
                //更改结束日期
                vip.setEndDate(calendar.getTime());
            }
            vip.setDays(vip.getDays()+SystemConfig.VIP_DAYS);
            vip.setUtime(new Date());
            return dao.updateById(vip);
        }
    }

    @Override
    public R queryByUid(String token) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        LambdaQueryWrapper<UserVip> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserVip::getUid,uid);
        return R.ok(dao.selectOne(wrapper));
    }

    @Override
    public boolean checkVip(String uid) {
        LambdaQueryWrapper<UserVip> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserVip::getUid,uid).ge(UserVip::getEndDate, DateUtil.getToday());
        return dao.selectOne(wrapper)!=null;
    }

    private long diffDay(Date d1){
        LocalDateTime start = LocalDateTimeUtil.now();
        LocalDateTime end = LocalDateTimeUtil.of(d1);
        return LocalDateTimeUtil.between(start, end).toDays();
    }
}

