package com.feri.co.auth.controller;

import com.feri.co.auth.service.UserLogService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 2.用户日志表(UserLog)表控制层
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@RestController
@RequestMapping("/server/userlog/")
public class UserLogController{
    /**
     * 服务对象
     */
    @Resource
    private UserLogService service;

    @GetMapping("all")
    public R all(HttpServletRequest request){
        return service.queryByUid(request.getHeader(SystemConfig.HEADER_TOKEN));
    }

}

