package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.Coupon;

/**
 * 10.优惠券表(Coupon)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
public interface CouponDao extends BaseMapper<Coupon> {

}

