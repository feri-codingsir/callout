package com.feri.co.auth.listener;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.feri.co.auth.dao.UserCouponDao;
import com.feri.co.auth.dao.UserDao;
import com.feri.co.auth.dao.UserVipDao;
import com.feri.co.auth.entity.Coupon;
import com.feri.co.auth.entity.UserCoupon;
import com.feri.co.auth.util.DateUtil;
import com.feri.co.auth.util.MyThreadPool;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.config.SystemConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/10 16:05
 */
@Component
@Slf4j
public class CouponSendListener {
    @Resource
    private UserVipDao vipDao;
    @Resource
    private UserDao userDao;
    @Resource
    private UserCouponDao userCouponDao;

    List<Integer> ids;
    @RabbitListener(queues = RabbitMQConfig.COUPONS_SEND)
    public void handler(Coupon coupon){
        log.info("开始主动发送优惠券:{}", System.currentTimeMillis());
        if(coupon.getCtype()!=1){
            //自动发送 要么VIP 要么所有人
            //2.查询需要发送的人
            List<Integer> uids;
            if(coupon.getCtype()==2){
                //vip专属
                uids=vipDao.selectUids(DateUtil.getToday());
            }else {
                //所有用户
                uids=userDao.selectIds();
            }
            //3.批量新增到数据库
            //逐条添加  性能最低
//            uids.forEach(id->{
//                userCouponDao.insert(new UserCoupon(id,coupon.getId()));
//            });
//            //一次性批量新增
//            userCouponDao.insertBatch(uids,coupon.getId());
            //使用线程池充分使用服务器资源 约到1个任务不能10万条
            if(uids.size()<=SystemConfig.COUPONS_ADDS){
                userCouponDao.insertBatch(uids,coupon.getId());
            }else {
                //数量太大 需要线程池 分片处理
                int tasks=uids.size()/SystemConfig.COUPONS_ADDS;
                tasks=uids.size()%SystemConfig.COUPONS_ADDS==0?tasks:tasks+1;
                //uids 数量 108万 11个任务
                //1个任务  0-10万-1
                //2个任务  10万-20万-1
                //……
                //11个任务 1000001-108万
                for (int i = 0; i <tasks ; i++) {
                    ids=new ArrayList<>();
                    if(i==tasks-1){
                        ids=uids.subList(i*SystemConfig.COUPONS_ADDS,uids.size()-1);
                    }else {
                        ids=uids.subList(i*SystemConfig.COUPONS_ADDS,(i+1)*SystemConfig.COUPONS_ADDS-1);
                    }
                    //添加线程池的任务 每个任务执行数量不超过10万条数据
                    MyThreadPool.addTask(userCouponDao,ids,coupon.getId());
                }
            }
        }
        log.info("结束主动发送优惠券:{}", System.currentTimeMillis());

    }
}
