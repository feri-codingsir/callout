package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 7.用户vip表(UserVip)表实体类
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@Data
@NoArgsConstructor
@TableName("t_user_vip")
public class UserVip extends Model<UserVip> {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //开始日期
    private Date startDate;
    //结束日期
    private Date endDate;
    //总天数
    private Integer days;
    //创建时间
    private Date ctime;
    //更新时间
    private Date utime;

    public UserVip(Integer uid, Date startDate, Date endDate, Integer days) {
        this.uid = uid;
        this.startDate = startDate;
        this.endDate = endDate;
        this.days = days;
        this.ctime=new Date();
        this.utime=new Date();
    }
}
