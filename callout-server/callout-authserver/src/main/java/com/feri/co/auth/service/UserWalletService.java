package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.UserWallet;
import com.feri.co.common.vo.R;

/**
 * 6.用户钱包表(UserWallet)表服务接口
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
public interface UserWalletService{

    /**
     * 钱包充值*/
    int add(int uid,double money);

    /**
     * 查询指定用户的钱包信息*/
    R queryByToken(String token);

    /**
     * 查询余额*/
    double queryMoney(String uid);
    /**
     * 更改余额
     * 扣款 <0
     * 加钱 >0*/
    int updateMoney(int uid,double money);


}

