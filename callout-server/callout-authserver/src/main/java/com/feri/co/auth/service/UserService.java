package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.User;
import com.feri.co.common.dto.FindUserDto;
import com.feri.co.common.dto.LoginDto;
import com.feri.co.common.dto.UserAddDto;
import com.feri.co.common.vo.R;

/**
 * 1.用户表(User)表服务接口
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
public interface UserService{
    /**
     * 校验昵称是否存在*/
    R checkName(String name);

    /**
     * 校验手机号是否存在*/
    R checkPhone(String phone);

    /**
     * 注册 新用户 手机号 昵称 密码*/
    R register(UserAddDto dto);

    /**
     * 账号(昵称或手机号)密码 登录*/
    R loginPass(LoginDto dto);

    /**
     * 手机号验证码登录
     * 手机号不是用户，自动注册*/
    R loginCode(LoginDto dto);

    /**
     * 退出*/
    R logout(String token);

    /**
     * 密码找回 手机号+验证码+新密码*/
    R findPass(FindUserDto dto);

    /**
     * 密码修改*/
    R updatePass(String token,String pass);


    /**
     * 查询全部用户*/
    R all();
    /**
     * 更新头像地址*/
    R updateImg(String token,String url);
    /**
     * 查询我邀请的用户*/
    R queryInviters(String token);
}

