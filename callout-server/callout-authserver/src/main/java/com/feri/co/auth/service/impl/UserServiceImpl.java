package com.feri.co.auth.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.UserDao;
import com.feri.co.auth.dao.UserInviterDao;
import com.feri.co.auth.dao.UserLogDao;
import com.feri.co.auth.entity.User;
import com.feri.co.auth.entity.UserInviter;
import com.feri.co.auth.entity.UserLog;
import com.feri.co.auth.service.UserService;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.FindUserDto;
import com.feri.co.common.dto.LoginDto;
import com.feri.co.common.dto.UserAddDto;
import com.feri.co.common.vo.R;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 1.用户表(User)表服务实现类
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao dao;
    @Resource
    private UserLogDao logDao;
    @Resource
    private StringRedisTemplate template;
    @Resource
    private UserInviterDao inviterDao;

    @Override
    public R checkName(String name) {
        //1校验
        if(StringUtils.hasLength(name)){
            //2.查询
            LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
            wrapper.eq(User::getNickname,name);
            //3.查询结果
            if(dao.selectOne(wrapper)==null){
                return R.ok();
            }else {
                return R.fail("亲，昵称已存在！");
            }
        }
        return R.fail("亲，参数有误！");
    }

    @Override
    public R checkPhone(String phone) {
        //1校验
        if(StringUtils.hasLength(phone)){
            //2.查询
            LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
            wrapper.eq(User::getPhone,phone);
            //3.查询结果
            if(dao.selectOne(wrapper)==null){
                return R.ok();
            }else {
                return R.fail("亲，手机号已存在！");
            }
        }
        return R.fail("亲，参数有误！");
    }

    @Override
    public R register(UserAddDto dto) {
        //1.校验
        if(dto!=null){
            //2.重新校验昵称或手机号
            LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
            wrapper.eq(User::getNickname,dto.getNickname()).or().eq(User::getPhone,dto.getPhone());
            wrapper.last(" limit 1");
            if(dao.selectOne(wrapper)==null){
                //3.查询 手机号 验证码是否校验成功
                if(template.opsForSet().isMember(RedisKeyConfig.CHECKED_PHONES,dto.getPhone())){
                    //4.密码转为密文
                    User user=new User();
                    BeanUtils.copyProperties(dto,user);
                    user.init();
                    user.setPassword(SecureUtil.sha1(SecureUtil.md5(dto.getPassword())));
                    //随机生成一个邀请码
                    user.setInviteCode(System.currentTimeMillis()+"-"+RandomUtil.randomString(6));
                    //5.操作数据库
                    if(dao.insert(user)>0){
                        //注册成功 新用户完成注册
                        //6.注册成功 删除之前记录的校验通过手机号
                        template.opsForSet().remove(RedisKeyConfig.CHECKED_PHONES,dto.getPhone());
                        //记录日志
                        logDao.insert(new UserLog(user.getId(), SystemConfig.UL_ADD,"用户完成了注册！"));
                        //校验当前用户是否通过邀请码注册，如是就需要新增一个邀请记录
                        if(StringUtils.hasLength(dto.getInviteCode())){
                            LambdaQueryWrapper<User> wrapper1=new LambdaQueryWrapper<>();
                            wrapper1.eq(User::getInviteCode,dto.getInviteCode());
                            User user1=dao.selectOne(wrapper1);
                            if(user1!=null){
                                inviterDao.insert(new UserInviter(user.getId(),user1.getId()));
                            }
                        }
                        return R.ok();
                    }
                }else {
                    return R.fail("亲，手机号验证码未校验！");
                }
            }else {
                return R.fail("亲，昵称或手机号已存在！");
            }
        }
        return R.fail();
    }

    @Override
    public R loginPass(LoginDto dto) {
        //1.校验
        if(dto!=null){
            //2.查询数据库
            LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
            wrapper.eq(User::getPhone,dto.getAccount()).or().eq(User::getNickname,dto.getAccount()).
                    eq(User::getPassword, SecureUtil.sha1(SecureUtil.md5(dto.getPassword()))).eq(User::getFlag,1);
            User user=dao.selectOne(wrapper);
            if(user!=null){
                //3.校验采用唯一登录
                if(template.hasKey(RedisKeyConfig.AUTH_LOGIN+user.getId())){
                    //禁止登录，保证唯一登录
                    return R.fail("亲，你的账号已在其他地方登录！");
                }else {
                    //4.登录成功  生成令牌
                    Map<String, Object> map = new HashMap<>();
                    map.put("uid", user.getId());
                    //令牌生成使用jwt算法
                    String token = JWTUtil.createToken(map, "cosystem".getBytes());
                    //5.记录 令牌和在线的用户
                    //记录令牌对应的uid,将来可以通过令牌校验或获取用户
                    template.opsForValue().set(RedisKeyConfig.AUTH_TOKEN + token, user.getId().toString(), 12, TimeUnit.HOURS);
                    //记录当前在线的uid,值记录令牌，可以实现唯一登录
                    template.opsForValue().set(RedisKeyConfig.AUTH_LOGIN + user.getId(), token, 12, TimeUnit.HOURS);
                    //记录日志
                    logDao.insert(new UserLog(user.getId(), SystemConfig.UL_LOGIN,"账号和密码实现登录！"));
                    return R.ok(token);
                }
            }
        }
        return R.fail("亲，账号或密码错误！");
    }

    @Override
    public R loginCode(LoginDto dto) {
        //如果手机号不存在就直接注册
        //存在直接登录
        //1.校验
        if(dto!=null){
            if(StringUtils.hasLength(dto.getAccount()) && StringUtils.hasLength(dto.getPassword())){
                //2.查询
                LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
                wrapper.eq(User::getPhone,dto.getAccount());
                User user=dao.selectOne(wrapper);
                if(user==null){
                    //3.不存在，需要自动注册
                    user=new User();
                    user.init();
                    user.setPhone(dto.getAccount());
                    user.setNickname(dto.getAccount());
                    //默认密码手机号后六位
                    user.setPassword(SecureUtil.sha1(SecureUtil.md5(dto.getAccount().substring(dto.getAccount().length()-6))));
                    dao.insert(user);
                    logDao.insert(new UserLog(user.getId(), SystemConfig.UL_ADD,"手机号验证码登录时，自动完成了注册！"));
                }
                //4.验证 短信验证码
                if(template.hasKey(RedisKeyConfig.SMS_LCODE+dto.getAccount())){
                    //5.比较验证码
                    if(template.opsForValue().get(RedisKeyConfig.SMS_LCODE+dto.getAccount()).equals(dto.getPassword())){
                        //6.校验 账号是否已在线
                        if(template.hasKey(RedisKeyConfig.AUTH_LOGIN + user.getId())){
                            return  R.fail("亲，你的账号已在其他地方登录！");
                        }else {
                            //可以登录
                            //7.登录成功  生成令牌
                            Map<String, Object> map = new HashMap<>();
                            map.put("uid", user.getId());
                            String token = JWTUtil.createToken(map, "cosystem".getBytes());
                            //8.记录 令牌和在线的用户
                            template.opsForValue().set(RedisKeyConfig.AUTH_TOKEN + token, user.getId().toString(), 12, TimeUnit.HOURS);
                            template.opsForValue().set(RedisKeyConfig.AUTH_LOGIN + user.getId(), token, 12, TimeUnit.HOURS);
                            //9.删除短信验证码
                            template.delete(RedisKeyConfig.SMS_LCODE+dto.getAccount());
                            logDao.insert(new UserLog(user.getId(), SystemConfig.UL_LOGIN,"手机号验证码实现登录！"));

                            return R.ok(token);
                        }
                    }
                }
            }
        }
        return R.fail("亲，手机号或验证码错误！");
    }

    @Override
    public R logout(String token) {
        //1.校验
        if(StringUtils.hasLength(token)){
            //2.校验令牌是否有效
            if(template.hasKey(RedisKeyConfig.AUTH_TOKEN+token)){
                String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
                //3.删除
                template.delete(RedisKeyConfig.AUTH_LOGIN+uid);
                template.delete(RedisKeyConfig.AUTH_TOKEN+token);
                return R.ok();
            }
        }
        return R.fail("亲，无效的令牌！");
    }

    @Override
    public R findPass(FindUserDto dto) {
        //1.校验
        if(dto!=null){
            //2.校验 密码找回短信验证码
            if(template.hasKey(RedisKeyConfig.SMS_FCODE+dto.getPhone())){
                //3.比较
                if(template.opsForValue().get(RedisKeyConfig.SMS_FCODE+dto.getPhone()).equals(dto.getCode())){
                    //4.修改数据库密码
                    String pass=SecureUtil.sha1(SecureUtil.md5(dto.getPass()));
                    if(dao.updatePass(dto.getPhone(),pass)>0){
                        //修改成功 如果存在在线，需要强制下线

                        return R.ok();
                    }
                }
            }
        }
        return R.fail();
    }

    @Override
    public R updatePass(String token, String pass) {
        //一定在线的用户
        //1.校验
        if(StringUtils.hasLength(pass)){
            //2.修改
            String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
            //3.修改
            pass=SecureUtil.sha1(SecureUtil.md5(pass));
            User user=new User();
            user.setId(Integer.parseInt(uid));
            user.setPassword(pass);
            user.setUtime(new Date());
            if(dao.updateById(user)>0){
                //修改成功
                logDao.insert(new UserLog(user.getId(),3,"修改了密码"));
                //强制下线
                template.delete(RedisKeyConfig.AUTH_TOKEN+token);
                template.delete(RedisKeyConfig.AUTH_LOGIN+uid);

                return R.ok();
            }
        }

        return R.fail();
    }

    @Override
    public R all() {
        return R.ok(dao.selectList(null));
    }

    @Override
    public R updateImg(String token, String url) {
        if(StringUtils.hasLength(url)) {
            String uid = template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN + token);
            User user = new User();
            user.setId(Integer.parseInt(uid));
            user.setImgurl(url);
            user.setUtime(new Date());
            if (dao.updateById(user) > 0) {
                return R.ok();
            }
        }
        return R.fail("亲，请检查，更新头像失败！");
    }

    @Override
    public R queryInviters(String token) {
        String uid=template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token);
        return R.ok(inviterDao.selectAll(Integer.parseInt(uid)));
    }
}

