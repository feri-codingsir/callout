package com.feri.co.auth.pay;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.*;
import com.alipay.api.response.*;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/9 11:22
 */
public class AliPayUtil {
    private static final String APPID="2017091608770636";
    private static final String PRIVATE_KEY="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCh1qI8uo1qhrcePsa5JUAoYUX8HfPuBt7kc90aCP1M/v61/uzaU/lyGQeChKV3jdDTn2Lcq6kT5JBl3TLiaYHmO6cId1nQAIUxiT9zhB9crc4wAx8CVabMbcqUefs7Xsp+YhhUgU5X6GOS3emkUeL7RegBnL8vayfEBeUDgBxsk/K/VygBA8sapsEhnoOrB6bhMY4GaJrxb0kg9Ej8x4kpExLcxkT+UgcOiJvh6vpBZo5CJsiPQkFvSsNsWY2uSDudSL/KqpMxz+yPfVvZDt4fOfyi+CfYR43Jlo4tsT7joqH2JT06BH+KdJyc1D3Lqw7w/WdmZtmoLghH0kRZawrLAgMBAAECggEAYYtpm+rhQ7zQ8HTr+DogknYW5Z/0H5qai93d/Uw/yEHFqlJt1iZZKlE1upBS311l6beesdzxeuD/u7X4bokjV27K/YpaYsl9fl74FJslAApuRXgMH68aawsd2CIxsBYxPL3JZl3Np6SVJ7eDlJwakFMRRK+CeIVAoaDf6R01hKctkYnnE0wT+ffQNKWsISoEyiKVT3g5fur7iPOuDlDXsfi6Mm+e75wCXTmRRHmb8lPBAMLV+Kj5DFxg8dwNz81Fs4ZM2Aq0lBaTfy1H1zSlM1m42wcsMYDcgdEH9aq+OgqK+cny6umgs7/Alg7IgV/9b7AhKdvAqLy2ERUJtooj2QKBgQDeIoDW3HuTq7sBaBnu63f7icT2RM3fApfOiGM4UDtxPvc5dS5S//o3E8p+rbp21FfBeyLOJFd9dg/eu+ETA+63QMPw4Kq4AH/EA5AFohaOQ0IKFDjYyxfyD8ajA4USDwdiaW2/vmMeAtGSv+W5zWb9/t49LOTwzEW904+yOGcmhQKBgQC6guDZ0Ob4o9nx5XwZXEe2di4MupARHceGzmolyDvs3Qi/w+8QntrDvfqIJoqoxOG5NVi3jtjkqtJtMaPyxqNWTabWOOTLbrsqlvPUmeCl0j3FVFKAGcV7/b9XkLvh1DtnIe6rhhZCVB4e4bL/katpOTgulhmSMaWIaztGU0F1DwKBgQCTeobdn/6vuSlsMqhdFppPN1W8R0wDjt4o8iYlwibk9e//hswdsPN307zyQ/dzY2FsBIvEHx6zHkpFD6nMDSVVJzuv1gmiJjqtccwR4V5mT0MuG+TuElCwlkbD/ddAeRfm/6Ys0oNN7oMjkiI8LKH/alI0fXT2Zji7YhWaNpZNXQKBgEU6q0duWS1VdGJrcgLf0+aQO0uSPEN+MD+Dgrb/ee7TpJm5mpUqwb0CWWoMFE/MtJRQjtujdDJ8jZrmYBqPTLWOIS1G9PXl5idK3Lq/Wzlxrmf+gpj19+2sJEfWe0a5xkrjt3mHTd/U5VFFKXHfmiZ2jLoOEPPI5c6bLudNo/BVAoGBAMvwRxLO4xb11Ip4rnEHkw3Qn8lrddoC3/m7haHYZ5DyGe8wdCdEi6wyk5MvlNQdqdVg5bqV0AiotIBcd5Pemabun2WaB11h/6SSb6wKY4Fnz+H155zaEww4no9BTG9llqQV7H8AS77dN1bxhcpE/MGFoB9JFU0D+BwXAnth4z1u";
    public static final String ALIPAY_PUBLIC_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx7jJT+PSEM6ZiimTW0SGUfg4cJU04H/mQqkL2mk7KaHXFQqMh4US6xYkDlaEXzOOfxevuBqWOaB4/8TleO1CHZHXWHu9Xc+iYtJPNJGrxoGLM+6Cg9IafJTygRoaqdH0SoVMpxFdOpUftNdXHO+G0ZpS/7c1zpn8G64zN5J17IFrLcUlsEnSgOrJxsS2Q50b44er0KQlj76pehB2sTveHS2vdhqXzrv+oq99XtUKEY1a3nwDjXneI7YYKLHD9KU53pti/ibLDkOEjO4+DRowd+wfSwkmWGVL3X320mvCfrg/aMN71B/cyyhW0mQ4cxqh2UcnpxLm0v/+uC7dSCyAJwIDAQAB";
    /**
     * 这里将来需要换成你们自己的回调地址*/
    private static final String notifyurl="https://y192580h27.goho.co/api/co/alipaycb";
    private static AlipayClient client;
    static {
        client=new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",APPID,PRIVATE_KEY,
                "JSON","UTF-8",ALIPAY_PUBLIC_KEY,"RSA2");
    }


    /**
     * 生成支付宝支付页面信息
     * @param no 订单号 唯一
     * @param money 订单金额 模拟1分钱
     * @param title 订单标题
     * @return 支付页面内容*/
    public static String createPay(String no,String title,double money) {
        //1.创建请求对象
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //2.设置支付参数
        //异步接收地址，仅支持http/https，公网可访问,监听订单支付成功，支付包主动调用
        request.setNotifyUrl(notifyurl);
        JSONObject bizContent = new JSONObject();
        //商户订单号，商家自定义，保持唯一性
        bizContent.put("out_trade_no", no);
        //支付金额，最小值0.01元
        //bizContent.put("total_amount",money);
        bizContent.put("total_amount", 0.01);
        //订单标题，不可使用特殊符号
        bizContent.put("subject", title);
        //电脑网站支付场景固定传值FAST_INSTANT_TRADE_PAY
        bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");
        request.setBizContent(bizContent.toString());
        //3.请求支付 获取响应
        try {
            AlipayTradePagePayResponse response = client.pageExecute(request);
            if(response.isSuccess()){
                //返回支付信息
                return response.getBody();
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 发起退款操作
     * @param no 订单号
     * @param money 退款金额
     * @return 退款是否成功*/
    public static boolean refoundPay(String no,double money){
        //1.创建请求对象
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        //2.设置请求参数信息
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", no);
        bizContent.put("refund_amount", money);
        request.setBizContent(bizContent.toString());
        try {
            //3.发起请求 获取响应
            AlipayTradeRefundResponse response = client.execute(request);
            if(response.isSuccess()){
                return response.getFundChange().equals("Y");
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return false;
    }
    /**
     * 实现支付的查询
     * @param no 订单号
     * @return 支付结果*/
    public static String queryPay(String no){
        //1创建请求对象
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        //2设置查询的订单号
        request.setBizContent("{\"out_trade_no\":\""+no+"\"}");
        //3.发起请求获取响应
        try {
            AlipayTradeQueryResponse response = client.execute(request);
            if(response.isSuccess()){
                return response.getTradeStatus();
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 关闭支付，未支付的才可以关闭
     * @param no 订单号
     * @return 是否关闭成功*/
    public static boolean closePay(String no){
        //1.创建请求对象
        AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
        //2.设置请求参数
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", no);
        request.setBizContent(bizContent.toString());
        try {
            AlipayTradeCloseResponse response = client.execute(request);
            if(response.isSuccess()){
                return response.getMsg().equals("Success");
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return false;
    }
    /**
     * 请求对账单
     * T+1 进行对账
     * @param date 要对账的日期
     * @return 对账单下载地址*/
    public static String queryBill(String date){
        //实例化请求对象
        AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
        //设置请求参数
        request.setBizContent("{\"bill_type\":\"trade\",\"bill_date\":\""+date+"\"}");
        try {
            //发起请求 获取响应
            AlipayDataDataserviceBillDownloadurlQueryResponse response = client.execute(request);
            if(response.isSuccess()){
                //返回下载地址
                return response.getBillDownloadUrl();
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

}
