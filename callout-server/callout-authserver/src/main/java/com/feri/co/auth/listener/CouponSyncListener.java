package com.feri.co.auth.listener;

import com.feri.co.auth.dao.CouponDao;
import com.feri.co.auth.entity.Coupon;
import com.feri.co.common.config.RabbitMQConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/11 10:11
 */
@Component
@Slf4j
public class CouponSyncListener {
    @Resource
    private CouponDao dao;
    @RabbitListener(queues = RabbitMQConfig.COUPONS_SYNC)
    public void hanlder(Coupon coupon){
        log.info("开始同步优惠券余量：{}",System.currentTimeMillis());
        coupon.setUtime(new Date());
        dao.updateById(coupon);
        log.info("结束同步优惠券余量：{}",System.currentTimeMillis());
    }
}
