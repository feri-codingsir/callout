package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 2.用户日志表(UserLog)表实体类
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@Data
@TableName("t_user_log")
@NoArgsConstructor
public class UserLog extends Model<UserLog> {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //类型 1注册2登录 3修改密码 4.vip充值 5.钱包充值
    private Integer type;
    //操作信息
    private String info;
    //创建时间
    private Date ctime;

    public UserLog(Integer uid, Integer type, String info) {
        this.uid = uid;
        this.type = type;
        this.info = info;
        this.ctime=new Date();
    }
}
