package com.feri.co.auth.service;

import com.feri.co.common.vo.R;

/**
 * 7.用户vip表(UserVip)表服务接口
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
public interface UserVipService{
    /**
     * 开通 或 续费 只支持年度VIP 一次只能买1年（365）*/
    int save(int uid);
    /**
     * 查询用户的VIP信息*/
    R queryByUid(String token);
    /**
     * 查询用户是否为vip
     * @return true 是，VIP false 不是*/
    boolean checkVip(String uid);
}
