package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.OssLog;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 4.对象存储表(OssLog)表服务接口
 *
 * @author Feri
 * @since 2023-08-09 09:36:13
 */
public interface AliOssService{
    /**
     * 上传图片*/
    R uploadImg(MultipartFile file) throws IOException;
    /**
     * 上传视频*/
    R uploadVideo(MultipartFile file) throws IOException;
    /**
     * 查询全部*/
    R all(PageBo bo);
}

