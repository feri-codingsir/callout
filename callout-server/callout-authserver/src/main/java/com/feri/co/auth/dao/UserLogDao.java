package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.UserLog;

/**
 * 2.用户日志表(UserLog)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
public interface UserLogDao extends BaseMapper<UserLog> {

}

