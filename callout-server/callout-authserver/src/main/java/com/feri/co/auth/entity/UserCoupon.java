package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 11.用户优惠券表(UserCoupon)表实体类
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@Data
@TableName("t_coupon")
@NoArgsConstructor
public class UserCoupon {
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户表id
    private Integer uid;
    //优惠券id
    private Integer cid;
    //标记位 1 未使用 2 已使用
    private Integer flag;
    //更新时间
    private Date ctime;
    //创建时间
    private Date utime;

    public UserCoupon(Integer uid, Integer cid) {
        this.uid = uid;
        this.cid = cid;
        this.flag=1;
        this.ctime=new Date();
        this.utime=new Date();
    }
}
