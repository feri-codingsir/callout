package com.feri.co.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.UserLogDao;
import com.feri.co.auth.dao.UserWalletDao;
import com.feri.co.auth.entity.UserLog;
import com.feri.co.auth.entity.UserWallet;
import com.feri.co.auth.service.UserWalletService;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.vo.R;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 6.用户钱包表(UserWallet)表服务实现类
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@Service
public class UserWalletServiceImpl  implements UserWalletService {
    @Resource
    private UserWalletDao dao;
    @Resource
    private StringRedisTemplate template;
    @Resource
    private UserLogDao logDao;
    @Override
    public int add(int uid, double money) {
        //1.查询 是否存在钱包数据
        LambdaQueryWrapper<UserWallet> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserWallet::getUid,uid);
        UserWallet wallet=dao.selectOne(wrapper);
        if(wallet==null){
            //第一次充值 新增
            wallet=new UserWallet(uid,money);
            if(dao.insert(wallet)>0){
                logDao.insert(new UserLog(wallet.getUid(),5,"恭喜你，第一次充值余额成功，期待你接下来的消费！"));
                return 1;
            }
        }else {
            //之前有过钱包数据 本次就是修改
            wallet.setBalance(wallet.getBalance()+money);
            wallet.setTotalBalance(wallet.getTotalBalance()+money);
            wallet.setUtime(new Date());
            if(dao.updateById(wallet)>0){
                logDao.insert(new UserLog(wallet.getUid(),5,"欢迎你续费，充值余额成功，期待你接下来的消费！"));
                return 1;
            }
        }
        return 0;
    }

    @Override
    public R queryByToken(String token) {
        LambdaQueryWrapper<UserWallet> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(UserWallet::getUid,template.opsForValue().get(RedisKeyConfig.AUTH_TOKEN+token));
        return R.ok(dao.selectOne(wrapper));
    }

    @Override
    public double queryMoney(String uid) {
        return dao.selectMoney(uid);
    }

    @Override
    public int updateMoney(int uid, double money) {
        return dao.updateMoney(money, uid);
    }
}

