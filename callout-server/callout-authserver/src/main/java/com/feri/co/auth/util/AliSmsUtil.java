package com.feri.co.auth.util;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import  com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.teautil.models.RuntimeOptions;
/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/5/4 16:24
 */
public class AliSmsUtil {
    private static Client client;
    /**
     * 密钥的id*/
    private final static String accessKeyId="LTAI5tLEfnQSsgsvPbegUnWM";
    /**
     * 密钥*/
    private final static String accessKeySecret="J6H14aiuycDfYnDQeCsYxy7hqbxy91";
    /**
     * 密码找回*/
    public final static String FIND_CODE="SMS_204975067";
    /**
     * 登录*/
    public final static String LOGIN_CODE="SMS_115250125";
    /**
     * 注册*/
    public final static String REGISTER_CODE="SMS_114390520";
    static {
        Config config = new com.aliyun.teaopenapi.models.Config()
                .setAccessKeyId(accessKeyId)
                .setAccessKeySecret(accessKeySecret);
        config.endpoint = "dysmsapi.aliyuncs.com";
        try {
            client=new Client(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送短信验证码
     * @param tem 模板
     * @param phone 手机号
     * @param code 验证码
     * @return 发送结果是否成功*/
    public static boolean sendSms(String tem,String phone,String code){
        //创建请求对象，设置发送信息
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers(phone)
                .setSignName("来自邢朋辉的短信")
                .setTemplateCode(tem)
                .setTemplateParam("{\"code\":\""+code+"\"}");
        //实例化 运行时对象
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            //执行请求 获取响应
            SendSmsResponse response=client.sendSmsWithOptions(sendSmsRequest, runtime);
            System.err.println("验证码："+code);
            //返回发送的结果
            return response.getBody().getCode().equals("OK");
        } catch (Exception error) {
            error.printStackTrace();
        }
        return false;
    }
}
