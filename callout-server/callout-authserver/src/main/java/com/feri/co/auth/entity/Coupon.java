package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * 10.优惠券表(Coupon)表实体类
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@Data
@TableName("t_coupon")
public class Coupon implements Serializable{
    //序号，主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //标记位 1 未审核 2审核通过 3审核拒绝
    private Integer flag;
    //发放：1.自动发放 2.主动领取
    private Integer sendType;
    //折扣
    private Double discount;
    //标题
    private String title;
    //发行数量 如果为0 说明人人都有
    private Integer num;
    //领取数量 自动发送
//    private Integer sendNum;
    //类型 1.无门槛劵 2.满减券 3.折扣券
    private Integer type;
    //满减券 最低金额
    private Integer minMoney;
    //抵扣的金额
    private Integer money;
    //领取的结束日期
    private Date endDate;
    //优惠券开始使用日期
    private Date formDate;
    //优惠券开始结束日期
    private Date toDate;
    //优惠券类型1.VIP会员自动发放 2.VIP专属 3.所有用户
    private Integer ctype;
    //描述信息
    private String info;
    //更新时间
    private Date utime;
    //创建时间
    private Date ctime;
    //优惠券图片
    private String imgUrl;
}
