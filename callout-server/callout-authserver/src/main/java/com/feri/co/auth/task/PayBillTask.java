package com.feri.co.auth.task;

import cn.hutool.http.HttpUtil;
import com.feri.co.auth.pay.AliPayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/9 14:44
 */
@Slf4j
@Component //IOC 创建对象
public class PayBillTask {
    //设置需要重复执行的方法，同时设置触发的时间 每隔3秒执行一次
    //CRON表达式 秒 分 时 日 月 星期 年   其中年可以省略，日和星期不能同时有值，其中必为? 占位符
//    @Scheduled(cron = "0/3 * * * * ?")
    public void test(){
        log.info("雪珂在查监控，喊喊醒醒：{}",System.currentTimeMillis());
    }
    //每晚11点开始昨天交易的对账
//    @Scheduled(cron = "0 0 23 * * ?")
    public void checkAliPay(){
        //1.获取昨天的日期
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,-1);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        //2.请求支付宝 获取昨日的交易订单
        String url= AliPayUtil.queryBill(sdf.format(calendar.getTime()));
        if(StringUtils.hasLength(url)){
            //3.请求支付宝对账单下载的url 下载内容
            String content= HttpUtil.get(url);
            if(StringUtils.hasLength(content)){
                //下载到昨天 支付宝的交易信息 然后数据库查询 昨日支付宝的交易订单
                //如果每一笔交易都可以核对上，说明今日对账成功，对账表 生成对账成功记录
                //如果某些交易未核对上，单独记得另一张表（交易对账异常表），明天再对账的时候，再对一次，如果连续3天交易还是对不上，需要人工处理


            }
        }
    }
}
