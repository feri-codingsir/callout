package com.feri.co.auth.pay;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/14 11:21
 */
public class BankPayUtil {

    /**
     * 生成银联支付页面信息
     * @param no 订单号 唯一
     * @param money 订单金额 模拟1分钱
     * @param title 订单标题
     * @return 支付页面内容*/
    public static String createPay(String no,String title,double money) {

        return null;
    }

    /**
     * 发起退款操作
     * @param no 订单号
     * @param money 退款金额
     * @return 退款是否成功*/
    public static boolean refoundPay(String no,double money){

        return false;
    }
    /**
     * 实现支付的查询
     * @param no 订单号
     * @return 支付结果*/
    public static String queryPay(String no){

        return null;
    }
    /**
     * 关闭支付，未支付的才可以关闭
     * @param no 订单号
     * @return 是否关闭成功*/
    public static boolean closePay(String no){

        return false;
    }
    /**
     * 请求对账单
     * T+1 进行对账
     * @param date 要对账的日期
     * @return 对账单下载地址*/
    public static String queryBill(String date){

        return null;
    }
}
