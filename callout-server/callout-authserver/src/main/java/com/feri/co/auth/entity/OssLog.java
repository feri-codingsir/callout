package com.feri.co.auth.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 4.对象存储表(OssLog)表实体类
 *
 * @author Feri
 * @since 2023-08-09 09:36:13
 */
@Data
@NoArgsConstructor
@TableName("t_oss_log")
public class OssLog extends Model<OssLog> {
    //序号
    @TableId(type = IdType.AUTO)
    private Integer id;
    //存储空间名
    private String bname;
    //对象名
    private String objName;
    //预览地址
    private String preUrl;
    //创建时间
    private Date ctime;
    //类型 1图片 2视频
    private Integer type;

    public OssLog(String bname, String objName, String preUrl, Integer type) {
        this.bname = bname;
        this.objName = objName;
        this.preUrl = preUrl;
        this.type = type;
        this.ctime=new Date();
    }

}
