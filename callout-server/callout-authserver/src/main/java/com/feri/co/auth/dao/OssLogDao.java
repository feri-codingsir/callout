package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.OssLog;

/**
 * 4.对象存储表(OssLog)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-09 09:36:12
 */
public interface OssLogDao extends BaseMapper<OssLog> {

}

