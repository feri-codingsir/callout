package com.feri.co.auth.controller;

import com.feri.co.auth.service.AliOssService;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/9 09:50
 */
@RestController
@RequestMapping("/server/oss/")
public class AliOssController {
    @Resource
    private AliOssService service;
    @PostMapping("uploadimg")
    public R uploadImg(MultipartFile file) throws IOException {
        return service.uploadImg(file);
    }
    @PostMapping("uploadvideo")
    public R uploadVideo(MultipartFile file) throws IOException {
        return service.uploadVideo(file);
    }
    @PostMapping("page")
    public R page(@RequestBody PageBo bo){
        return service.all(bo);
    }
}
