package com.feri.co.auth.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feri.co.auth.dao.SmsLogDao;
import com.feri.co.auth.entity.SmsLog;
import com.feri.co.auth.service.SmsLogService;
import com.feri.co.auth.util.AliSmsUtil;
import com.feri.co.common.config.RabbitMQConfig;
import com.feri.co.common.config.RedisKeyConfig;
import com.feri.co.common.dto.PhoneCodeDto;
import com.feri.co.common.mq.CodeMsg;
import com.feri.co.common.vo.R;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 3.短信发送记录(SmsLog)表服务实现类
 *
 * @author Feri
 * @since 2023-08-08 13:58:35
 */
@Service
public class SmsLogServiceImpl implements SmsLogService {
    @Resource
    private SmsLogDao dao;
    @Resource
    private StringRedisTemplate template;
    @Resource
    private RabbitTemplate rabbitTemplate;


    /**
     * 发送验证码
     * @param tem 短信模板code
     * @param phone 手机号
     * @param key Redis中记录验证码的key */
    private boolean sendCode(String tem,String phone,String key){
        //1.校验
        if(StringUtils.hasLength(tem) && StringUtils.hasLength(phone)) {
            //2.生成验证码 随机6位
            String code= RandomUtil.randomInt(100000,999999)+"";
            //3.发送短信 因为短信比较耗时，如果直接发送就会出现接口一直需要等待短信发送结束，为了防止等待 引入RabbitMQ
            rabbitTemplate.convertAndSend("", RabbitMQConfig.SMS_QUEUE,new CodeMsg(tem,phone,code,key));

            return true;
        }
        return false;
    }

    @Override
    public R sendRCode(String phone) {
        if(sendCode(AliSmsUtil.REGISTER_CODE,phone,RedisKeyConfig.SMS_RCODE)){
            return R.ok("请查阅手机，查看验证码！");
        }
        return R.fail();
    }

    @Override
    public R sendFCode(String phone) {
        if(sendCode(AliSmsUtil.FIND_CODE,phone,RedisKeyConfig.SMS_FCODE)){
            return R.ok("请查阅手机，查看验证码！");
        }
        return R.fail();
    }

    @Override
    public R sendLCode(String phone) {
        if(sendCode(AliSmsUtil.LOGIN_CODE,phone,RedisKeyConfig.SMS_LCODE)){
            return R.ok("请查阅手机，查看验证码！");
        }
        return R.fail();
    }

    @Override
    public R checkRCode(PhoneCodeDto dto) {
        //1.验证
        if(dto!=null){
            //2.校验是否存在验证码
            if(template.hasKey(RedisKeyConfig.SMS_RCODE+dto.getPhone())){
                //3.校验
                if(template.opsForValue().get(RedisKeyConfig.SMS_RCODE+dto.getPhone()).equals(dto.getCode())){
                    //4.验证码正确,删除
                    template.delete(RedisKeyConfig.SMS_RCODE+dto.getPhone());
                    //5.防止未进行验证码就直接进行注册
                    template.opsForSet().add(RedisKeyConfig.CHECKED_PHONES,dto.getPhone());
                    return R.ok();
                }
            }
        }
        return R.fail();
    }

    @Override
    public R all() {
        return R.ok(dao.selectList(null));
    }
}

