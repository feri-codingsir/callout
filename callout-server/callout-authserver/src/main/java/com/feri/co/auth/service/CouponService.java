package com.feri.co.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feri.co.auth.entity.Coupon;
import com.feri.co.common.dto.CouponAddDto;
import com.feri.co.common.dto.CouponUpdateDto;
import com.feri.co.common.vo.R;

/**
 * 10.优惠券表(Coupon)表服务接口
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
public interface CouponService{
    /**
     * 新增优惠券
     * */
    R save(CouponAddDto dto);
    /**
     * 审核优惠券
     * */
    R update(CouponUpdateDto dto);
    /**
     * 查询可以领取优惠券*/
    R queryList();

}

