package com.feri.co.auth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feri.co.auth.entity.PayLog;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 8.支付记录表(PayLog)表数据库访问层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
public interface PayLogDao extends BaseMapper<PayLog> {

    @Update("update t_pay_log set flag=#{flag} where no=#{no}")
    int updateFlag(@Param("no") String no,@Param("flag") int flag);
}

