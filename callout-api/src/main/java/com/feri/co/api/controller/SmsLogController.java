package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.dto.PhoneCodeDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 3.短信发送记录(SmsLog)表控制层
 *
 * @author Feri
 * @since 2023-08-08 13:58:29
 */
@RestController
@RequestMapping("/api/sms/")
@Api(tags = "短信操作")
public class SmsLogController{
    /**
     * 服务对象
     */
    @Resource
    private AuthService service;

    @PostMapping("checkrcode")
    public R checkRCode(@RequestBody PhoneCodeDto dto){
        return service.checkRCode(dto);
    }
    @PostMapping("sendrcode")
    public R sendRCode(String phone){
        return service.sendRCode(phone);
    }
    @PostMapping("sendlcode")
    public R sendLCode(String phone){
        return service.sendLCode(phone);
    }
    @PostMapping("sendfcode")
    public R sendFCode(String phone){
        return service.sendFCode(phone);
    }
    @GetMapping("all")
    public R all(){
        return service.allSms();
    }
}

