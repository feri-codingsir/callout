package com.feri.co.api.controller;

import com.feri.co.api.feign.CouponService;
import com.feri.co.common.dto.CouponAddDto;
import com.feri.co.common.dto.CouponUpdateDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 10.优惠券表(Coupon)表控制层
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@RestController
@RequestMapping("/api/coupon/")
@Api(tags = "优惠券操作")
public class CouponController {
    /**
     * 服务对象
     */
    @Resource
    private CouponService service;

    @PostMapping("save")
    public R save(@RequestBody CouponAddDto dto){
        return service.save(dto);
    }
    @PostMapping("update")
    public R update(@RequestBody CouponUpdateDto dto){
        return service.update(dto);
    }
    @GetMapping("all")
    public R all(){
        return service.all();
    }
}

