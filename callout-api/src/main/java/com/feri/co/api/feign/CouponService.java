package com.feri.co.api.feign;

import com.feri.co.api.config.OpenFeignConfig;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.CouponAddDto;
import com.feri.co.common.dto.CouponUpdateDto;
import com.feri.co.common.vo.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/10 17:18
 */
@FeignClient(value = "co-auth",configuration = OpenFeignConfig.class)
public interface CouponService {
    @PostMapping("/server/coupon/save")
    R save(@RequestBody CouponAddDto dto);
    @PostMapping("/server/coupon/update")
    R update(@RequestBody CouponUpdateDto dto);
    @GetMapping("/server/coupon/all")
    R all();
    @GetMapping("/server/usercoupon/add")
    R add(@RequestParam("cid") int cid);
    @GetMapping("/server/usercoupon/my")
    R myCoupons();
}
