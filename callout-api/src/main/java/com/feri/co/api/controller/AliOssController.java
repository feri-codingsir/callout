package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/9 09:50
 */
@RestController
@RequestMapping("/api/oss/")
@Api(tags = "OSS对象存储")
public class AliOssController {
    @Resource
    private AuthService service;
    @PostMapping("uploadimg")
    public R uploadImg(@RequestPart MultipartFile file) throws IOException {
        return service.uploadImg(file);
    }
    @PostMapping("uploadvideo")
    public R uploadVideo(@RequestPart MultipartFile file) throws IOException {
        return service.uploadVideo(file);
    }
    @GetMapping("page")
    public R page(PageBo bo){
        return service.page(bo);
    }
}
