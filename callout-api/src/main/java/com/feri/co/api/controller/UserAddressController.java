package com.feri.co.api.controller;

import com.feri.co.api.feign.ServeService;
import com.feri.co.common.dto.UserAddressAddDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
 * 16.用户服务地址(UserAddress)表控制层
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@RestController
@RequestMapping("/api/address/")
@Api(tags = "服务地址")
public class UserAddressController{
    /**
     * 服务对象
     */
    @Resource
    private ServeService service;
    @PostMapping("save")
    public R save(UserAddressAddDto dto){
        return service.saveAddress(dto);
    }
   /**
     * 查询全部*/
   @GetMapping("all")
    public R all(){
        return service.allAddresses();
    }
    /**
     * 查询默认*/
    @GetMapping("default")
    public R defaultOne(){
        return service.defaultOne();
    }
}
