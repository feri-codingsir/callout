package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 6.用户钱包表(UserWallet)表控制层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@RestController
@RequestMapping("/api/userwallet/")
@Api(tags = "用户钱包")
public class UserWalletController {
    /**
     * 服务对象
     */
    @Resource
    private AuthService service;

    @GetMapping("my")
    public R my(HttpServletRequest request){
        return service.wallet();
    }
}

