package com.feri.co.api.controller;

import com.feri.co.api.feign.ServeService;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.dto.ServeTypeAddDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Map;

/**
 * 12.服务类型表(ServeType)表控制层
 *
 * @author Feri
 * @since 2023-08-11 11:46:16
 */
@RestController
@RequestMapping("/api/servetype/")
@Api(tags = "服务类型")
public class ServeTypeController {
    /**
     * 服务对象
     */
    @Resource
    private ServeService service;
    @PostMapping("save")
    public R save(@RequestBody ServeTypeAddDto dto){
        return service.save(dto);
    }
    @PostMapping("del")
    public R del(@RequestBody Map<String,Integer> map){
        return service.del(map.get("id"));
    }
    @GetMapping("home")
    public R home(){
        return service.home();
    }
    @PostMapping("page")
    public R page(@RequestBody PageBo bo){
        return service.page(bo);
    }
}
