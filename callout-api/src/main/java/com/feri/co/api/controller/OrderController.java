package com.feri.co.api.controller;

import com.feri.co.api.feign.OrderService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.OrderAddDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 17.订单表(Order)表控制层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@RestController
@RequestMapping("/api/order/")
@Api(tags = "订单操作")
public class OrderController{
    /**
     * 服务对象
     */
    @Resource
    private OrderService service;
    /**
     * 购买-下单*/
    @PostMapping("save")
    public R buy(@RequestBody OrderAddDto dto, HttpServletRequest request){
        return service.buy(dto);
    }
    /**
     * 修改状态*/
    @GetMapping("update")
    public R update(String no,int flag){
        return service.update(no,flag);
    }
    /**
     * 查询某个用户的订单 根据订单状态*/
    @GetMapping("my")
    public R queryByUid(HttpServletRequest request){
        return service.queryMyOrder();
    }
    @GetMapping("prefix")
    public R queryPre(int type,int id){
        return service.queryPre(type, id);
    }
    @GetMapping("cancel")
    public R cancelOrder(String no){
        return service.cancelOrder(no);
    }
    @GetMapping("refound")
    public R refoundOrder(String no,String reason){
        return service.refoundOrder(no, reason);
    }


}
