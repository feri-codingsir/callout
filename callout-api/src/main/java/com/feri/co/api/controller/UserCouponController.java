package com.feri.co.api.controller;


import com.feri.co.api.feign.CouponService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 11.用户优惠券表(UserCoupon)表控制层
 *
 * @author Feri
 * @since 2023-08-10 15:07:33
 */
@RestController
@RequestMapping("/api/usercoupon/")
@Api(tags = "用户优惠券")
public class UserCouponController {
    /**
     * 服务对象
     */
    @Resource
    private CouponService service;

    @GetMapping("add")
    public R add(int cid){
        return service.add(cid);
    }
    @GetMapping("my")
    public R myCoupons(HttpServletRequest request){
        return service.myCoupons();
    }

}

