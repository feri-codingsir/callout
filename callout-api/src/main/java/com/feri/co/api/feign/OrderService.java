package com.feri.co.api.feign;

import com.feri.co.api.config.OpenFeignConfig;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.CartAddDto;
import com.feri.co.common.dto.CartUpdateNumDto;
import com.feri.co.common.dto.OrderAddDto;
import com.feri.co.common.vo.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/14 13:58
 */
@FeignClient(value = "co-order",configuration = OpenFeignConfig.class)
public interface OrderService {
    @PostMapping("/server/cart/add")
    R save(@RequestBody CartAddDto dto);
    @PostMapping("/server/cart/change")
    R updateNum(@RequestBody CartUpdateNumDto dto);
    @PostMapping("/server/cart/my")
    R queryByUid();
    @GetMapping("/server/cart/del")
    R del(@RequestParam("id") int id);

    @PostMapping("/server/order/save")
    R buy(@RequestBody OrderAddDto dto);
    @GetMapping("/server/order/update")
    R update(@RequestParam("no") String no,@RequestParam("flag") int flag);
    @GetMapping("/server/order/my")
    R queryMyOrder();
    @GetMapping("/server/order/prefix")
    R queryPre(@RequestParam("type") int type,@RequestParam("id") int id);
    @GetMapping("/server/order/cancel")
    R cancelOrder(@RequestParam("no") String no);
    @GetMapping("/server/order/refound")
    R refoundOrder(@RequestParam("no") String no,@RequestParam("reason") String reason);

    @GetMapping("/server/orderlog/logs")
    R logs(@RequestBody PageBo bo);
}
