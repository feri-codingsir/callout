package com.feri.co.api.feign;

import com.feri.co.api.config.OpenFeignConfig;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.ServeAddDto;
import com.feri.co.common.dto.ServeDetailAddDto;
import com.feri.co.common.dto.ServeTypeAddDto;
import com.feri.co.common.dto.UserAddressAddDto;
import com.feri.co.common.vo.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/14 13:58
 */
@FeignClient(value = "co-order",configuration = OpenFeignConfig.class)
public interface ServeService {
    /**
     * 新增
     **/
    @PostMapping("/server/serve/save")
    R save(@RequestBody ServeAddDto dto);
    /**
     * 删除
     **/
    @GetMapping("/server/serve/del")
    R del(@RequestParam("id") int id);
    /**
     * 查询 首页榜单*/
    @GetMapping("/server/serve/top")
    R queryTop(@RequestParam("stid") int stid);
    /**
     * 查询 根据类型 要是动态排序 分页*/
    @GetMapping("/server/serve/list")
    R queryList(@RequestParam("level") int level,@RequestParam("type") int type,@RequestParam("order") String order);
    /**
     * 查询 详情 热门服务 缓存*/
    @GetMapping("/server/serve/detail")
    R queryDetail(@RequestParam("id") int id);

    /**
     * 新增*/
    @PostMapping("/server/servedetail/save")
    R saveDetail(@RequestBody ServeDetailAddDto dto);
    /**
     * 上下架*/
    @GetMapping("/server/servedetail/updown")
    R updateFlag(@RequestParam("id") int id,@RequestParam("flag") int flag);
    /**
     * 删除 假删除 逻辑删除 修改标记位*/
    @GetMapping("/server/servedetail/del")
    R delDetail(@RequestParam("id") int id);


    @PostMapping("/server/servetype/save")
    R save(@RequestBody ServeTypeAddDto dto);
    @GetMapping("/server/servetype/home")
    R home();
    @PostMapping("/server/servetype/page")
    R page(@RequestBody PageBo bo);

    @PostMapping("/server/address/save")
    public R saveAddress(@RequestBody UserAddressAddDto dto);
    /**
     * 查询全部*/
    @GetMapping("/server/address/all")
    public R allAddresses();
    /**
     * 查询默认*/
    @GetMapping("/server/address/default")
    public R defaultOne();
}
