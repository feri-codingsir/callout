package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.dto.FindUserDto;
import com.feri.co.common.dto.LoginDto;
import com.feri.co.common.dto.UserAddDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 1.用户表(User)表控制层
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@RestController
@RequestMapping("/api/user/")
@Api(tags = "用户操作")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private AuthService service;

    @GetMapping("checkname")
    public R checkName(String name){
        return service.checkName(name);
    }
    @GetMapping("checkphone")
    public R checkPhone(String phone){
        return service.checkPhone(phone);
    }
    @PostMapping("logincode")
    public R loginCode(@RequestBody LoginDto dto){
        return service.loginCode(dto);
    }
    @PostMapping("loginpass")
    public R loginPass(@RequestBody LoginDto dto){
        return service.loginPass(dto);
    }
    @PostMapping("register")
    public R register(@RequestBody UserAddDto dto){
        return service.register(dto);
    }
    @GetMapping("logout")
    public R logout(){
        return service.logout();
    }
    @GetMapping("all")
    public R all(){
        return service.allUsers();
    }
    @PostMapping("findpass")
    public R findPass(@RequestBody FindUserDto dto){
        return service.findPass(dto);
    }
    @GetMapping("updatepass")
    public R updatePass(String pass){
        return service.updatePass(pass);
    }
    @GetMapping("updateimg")
    public R updateImg(String url){
        return service.updateImg(url);
    }
    @GetMapping("inviters")
    public R inviters(){
        return service.inviters();
    }
}
