package com.feri.co.api.controller;

import com.feri.co.api.feign.OrderService;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
 * 18.订单流水表(OrderLog)表控制层
 *
 * @author Feri
 * @since 2023-08-12 10:19:49
 */
@RestController
@RequestMapping("/api/orderlog/")
@Api(tags = "订单流水")
public class OrderLogController{
    /**
     * 服务对象
     */
    @Resource
    private OrderService service;

    @GetMapping("logs")
    public R logs(@RequestBody PageBo bo){
        return service.logs(bo);
    }
}
