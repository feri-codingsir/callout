package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 2.用户日志表(UserLog)表控制层
 *
 * @author Feri
 * @since 2023-08-08 13:58:37
 */
@RestController
@RequestMapping("/api/userlog/")
@Api(tags = "用户日志")
public class UserLogController{
    /**
     * 服务对象
     */
    @Resource
    private AuthService service;

    @GetMapping("all")
    public R all(HttpServletRequest request){
        return service.allLogs();
    }

}

