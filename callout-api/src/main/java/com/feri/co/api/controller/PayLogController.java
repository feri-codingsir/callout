package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.dto.PayDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 8.支付记录表(PayLog)表控制层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@RestController
@RequestMapping("/api/pay/")
@Api(tags = "支付操作")
public class PayLogController{
    /**
     * 服务对象
     */
    @Resource
    private AuthService service;

    @PostMapping("save")
    public R save(@RequestBody PayDto dto, HttpServletRequest request){
        return service.savePay(dto);
    }
    @GetMapping("all")
    public R all(HttpServletRequest request) {
        return service.allPays();
    }

}

