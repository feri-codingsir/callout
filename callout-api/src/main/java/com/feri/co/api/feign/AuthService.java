package com.feri.co.api.feign;

import com.feri.co.api.config.OpenFeignConfig;
import com.feri.co.common.bo.PageBo;
import com.feri.co.common.dto.*;
import com.feri.co.common.vo.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/8 16:07
 */
@FeignClient(value = "co-auth",configuration = OpenFeignConfig.class)
public interface AuthService {
    @PostMapping("/server/sms/checkrcode")
    R checkRCode(@RequestBody PhoneCodeDto dto);
    @PostMapping("/server/sms/sendrcode")
    R sendRCode(@RequestParam("phone") String phone);
    @PostMapping("/server/sms/sendlcode")
    R sendLCode(@RequestParam("phone") String phone);
    @PostMapping("/server/sms/sendfcode")
    R sendFCode(@RequestParam("phone") String phone);
    @GetMapping("/server/sms/all")
    R allSms();

    @GetMapping("/server/user/checkname")
    R checkName(@RequestParam("name") String name);
    @GetMapping("/server/user/checkphone")
    R checkPhone(@RequestParam("phone") String phone);
    @PostMapping("/server/user/logincode")
    R loginCode(@RequestBody LoginDto dto);
    @PostMapping("/server/user/loginpass")
    R loginPass(@RequestBody LoginDto dto);
    @PostMapping("/server/user/register")
    R register(@RequestBody UserAddDto dto);
    @GetMapping("/server/user/logout")
    R logout();
    @PostMapping("/server/user/findpass")
    R findPass(@RequestBody FindUserDto dto);
    @GetMapping("/server/user/updatepass")
    R updatePass(@RequestParam("pass") String pass);
    @GetMapping("/server/user/updateimg")
    R updateImg(@RequestParam("url") String url);
    @GetMapping("/server/user/all")
    R allUsers();
    @GetMapping("/server/user/inviters")
    R inviters();


    @GetMapping("/server/userlog/all")
    R allLogs();

    @PostMapping(value = "/server/oss/uploadimg",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    R uploadImg(@RequestPart("file") MultipartFile file);
    @PostMapping(value ="/server/oss/uploadvideo",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    R uploadVideo(@RequestPart("file") MultipartFile file);
    @PostMapping("/server/oss/page")
    R page(@RequestBody PageBo bo);

    @GetMapping("/server/vip/vip")
    R queryVip();

    @PostMapping("/server/pay/save")
    R savePay(@RequestBody PayDto dto);
    @GetMapping("/server/pay/all")
    R allPays();

    @GetMapping("/server/userwallet/my")
    R wallet();
}
