package com.feri.co.api.controller;

import com.feri.co.api.feign.OrderService;
import com.feri.co.common.dto.CartAddDto;
import com.feri.co.common.dto.CartUpdateNumDto;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
/**
 * 15.购物车表(Cart)表控制层
 * @author Feri
 * @since 2023-08-12 10:19:48
 */
@RestController
@RequestMapping("/api/cart/")
@Api(tags = "购物车")
public class CartController {
    /**
     * 服务对象
     */
    @Resource
    private OrderService service;


    @PostMapping("add")
    R save(@RequestBody CartAddDto dto){
        return service.save(dto);
    }
    /**
     * 修改数量*/
    @PostMapping("change")
    R updateNum(@RequestBody CartUpdateNumDto dto){
        return service.updateNum(dto);
    }
    /**
     * 查询某个用户购物车*/
    @PostMapping("my")
    R queryByUid(){
        return service.queryByUid();
    }
    /**
     * 删除 购物车内容*/
    @GetMapping("del")
    R del(int id){
        return service.del(id);
    }
}
