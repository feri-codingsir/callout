package com.feri.co.api.controller;

import com.feri.co.api.feign.AuthService;
import com.feri.co.common.config.SystemConfig;
import com.feri.co.common.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 7.用户vip表(UserVip)表控制层
 *
 * @author Feri
 * @since 2023-08-09 11:07:50
 */
@RestController
@RequestMapping("/api/vip/")
@Api(tags = "VIP用户操作")
public class UserVipController {
    /**
     * 服务对象
     */
    @Resource
    private AuthService service;

    @GetMapping("vip")
    public R queryUid(HttpServletRequest request){
        return service.queryVip();
    }

}
